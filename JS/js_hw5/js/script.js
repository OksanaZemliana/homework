'use strict'
// Теоретичні питання
// Опишіть своїми словами, що таке метод об'єкту
// Метод об'єкту це властивість даного об'єкту яка являється функцією

// Який тип даних може мати значення властивості об'єкта?
// Типи даних значення  можуть бути будь якими, як примітивними так і іншими об'єктами'

// Об'єкт це посилальний тип даних. Що означає це поняття?
// Посилальний тип даних - це коли зберігається не самі дані а адреса до них.

// Завдання
// Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання' +
// ' бібліотек типу jQuery або React.
//
// Технічні вимоги:
//     Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в
// нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль
// результат виконання функції.
//     Необов'язкове завдання підвищеної складності
// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери
// setFirstName() та setLastName(), які дозволять змінити дані властивості.

const createNewUser = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        get getLogin() {
            return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`
        },
        setFirstName(value) {
            Object.defineProperty(this, 'firstName', {writable: true});
            this.firstName = value;
        },
        setLastName(value) {
            Object.defineProperty(this, 'lastName', {writable: true});
            this.lastName = value;
        },
    }
}

const newUser = createNewUser(prompt('Enter your First Name'), prompt('Enter yor Last Name'));

console.log(newUser.getLogin);

Object.defineProperty(newUser, 'firstName', {writable: false});
Object.defineProperty(newUser, 'lastName', {writable: false});

//Test change name
newUser.setFirstName('Dora');
newUser.setLastName('Green');
console.log(newUser.getLogin);


