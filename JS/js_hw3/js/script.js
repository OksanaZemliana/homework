'use strict'
//
// 1.Описать своими словами для чего вообще нужны функции в программировании.
// 2.Описать своими словами, зачем в функцию передавать аргумент.
//
//1.Функции используются , чтобы не повторять один и тот же код в программе или обращаться к одному коду несколько раз.
//2.Аргументы в функции передается , для того чтобы присвоить их значения параметрам функции, в обратном случае
// значение будет неопределено.

// Технические требования:
//
//     Считать с помощью модального окна браузера два числа.
//     Считать с помощью модального окна браузера математическую операцию, которую нужно совершить.
//     Сюда может быть введено +, -, *, /.

// Создать функцию, в которую передать два значения и операцию.
//     Вывести в консоль результат выполнения функции.
//
//     Необязательное задание продвинутой сложности:
//
//     После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал
//     не числа, - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна
//     быть введенная ранее информация).


function checkUserNumber(userNumber) {
    while (!isFinite(userNumber)) {
        userNumber = +prompt('Enter your true number', userNumber);
    }
    return userNumber;
}

function checkUsersSign(userSign) {
    while (userSign !== "+" && userSign !== "-" && userSign !== "/" && userSign !== "*") {
        userSign = prompt('Enter your true sign', userSign);
    }
    return userSign;

}

const firstNumberUser = checkUserNumber(+prompt('Enter your first number'));
const secondNumberUser = checkUserNumber(+prompt('Enter your second number'));
const signUser = checkUsersSign(prompt('Enter your sign'));

function calcMathOperation(firstNumber, secondNumber, sign) {
    switch (sign) {
        case '+':
            return firstNumber + secondNumber;
        case '-':
            return firstNumber - secondNumber;
        case '*':
            return firstNumber * secondNumber;
        case '/':
            return firstNumber / secondNumber;
    }
}

console.log(calcMathOperation(firstNumberUser, secondNumberUser, signUser));
