'use strict'
/**
 * ЗАДАНИЕ 1
 *
 * Вывести в консоль:
 * - Максимальное положительное число;
 * - Минимальное отрицательное число;
 * - Максимальное допустимое числовое значение в JavaScript;
 * - Минимальное допустимое числовое значение в JavaScript;
 * - Не число.
 *
 * Заметка: в этом задании необходимо проявить смекалку
 * и умение искать решения нестандартных задач.
 */
// https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Number

/* Максимальное доступное в языке число с плавающей точкой (дробное). */

// console.log(Number.MAX_VALUE);
// console.log(Number.MIN_VALUE);
// console.log(Number.MAX_SAFE_INTEGER);
// console.log(Number.MIN_SAFE_INTEGER);
// console.log(Number.NaN);
//
// const firstNumber = +prompt('Enter your first number ');
// const secondNumber = +prompt('Enter your second number ');
// const thirdNumber = +prompt('Enter your third number ');
// if (isNaN(firstNumber) || isNaN(secondNumber) || isNaN(thirdNumber)) {
//     alert('⛔️ Ошибка! Все три введённых значения должны быть числами!')
// }
// else {
//     console.log((firstNumber + secondNumber + thirdNumber)/3);
// }
// Создать объект student, у которого будут такие свойства:
//     имя (name), фамилия (last name), коэффициент лени (laziness)
// равный 4 и коэффициент хитрости (trick) равный 5. Вывести его в консоль.

// const student = {
//     name: "Oksana",
//     lastName: "Zemliana",
//     laziness: 4,
//     trick: 5,
// }
// console.log(student);
//
// if (student.laziness >= 3 || student.laziness <= 5 && student.trick <= 4) {
//     console.log(`${student.name}  ${student.lastName}  enter to new examen`)
// }

// Создать объект danItStudent, у которого будут такие свойства: имя, фамилия, количество сданных домашних работ.
//     Вывести объект в консоль. После чего спросить у пользователя "Какое свойство вы хотите изменить?", и если у
// объекта такого свойства нет - переспрашивать с сообщением "имяСвойства у объекта нет. Напишите, пожалуйста, правильное" +
// " свойство объекта" пока пользователь не введет имя существующего свойства объекта или не нажмет Cancel.
//     Если пользователь не нажал Cancel, спросить
// "Какое значение вы хотите присвоить этому свойству?". Изменить требуемое свойство и вывести объект в консоль.

// const danItStudent = {
//     name : 'Oksana',
//     lastName: 'Zemliana',
//     quantityHomeWork : 30,
//
// }

// const user = {
//     name: "Олег",
//     "last name": "Дихтяренко",
//     patronymic: "Александрович",
//     getInitials: function() {
//         return this.name[0] + this["last name"][0] + this.patronymic[0];
//     }
// }
//
// console.log(user.getInitials());