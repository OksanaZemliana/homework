'use strict'
//Описать своими словами в несколько строчек, зачем в программировании нужны циклы.
//Циклы необходимы для повторения действий при выполнении определенных условий.

/*Считать с помощью модального окна браузера число, которое введет пользователь.
Вывести в консоли все числа кратные 5, от 0 до введенного пользователем числа.
Если таких чисел нету - вывести в консоль фразу `Sorry, no numbers'
Проверить, что введенное значение является целым числом. Если данное условие не соблюдается, повторять
вывод окна на экран до тех пор, пока не будет введено целое число.*/

let userNumber;
do {
    userNumber = +prompt('Enter your integer number');
} while (!userNumber || !Number.isInteger(userNumber));
if (userNumber < 5) {
    console.log('Sorry, no numbers');
} else
    for (let searchNumber = 5; searchNumber <= userNumber; searchNumber += 5) {
        console.log('Your integer number ' + searchNumber);
    }
//
/*Считать два числа, m и n. Вывести в консоль все простые числа в диапазоне от m до n
(меньшее из введенных чисел будет m, бОльшее будет n). Если хотя бы одно из чисел не
соблюдает условие валидации, указанное выше, вывести сообщение об ошибке,
и спросить оба числа заново.*/

let lessNumber = +prompt('Enter your less number');
let moreNumber = +prompt('Enter your more number');
while (lessNumber >= moreNumber && !lessNumber && !moreNumber) {
    alert('this is error');
    lessNumber = +prompt('Enter your true less number');
    moreNumber = +prompt('Enter your true more number');
}
labelPrime: for (; lessNumber <= moreNumber; lessNumber++) {
    if (lessNumber < 2) {
        lessNumber = 2;
    }
    for (let two = 2; two < lessNumber; two++) {
        if (lessNumber % two === 0) continue labelPrime;

    }
    console.log('Your simple number ' + lessNumber);
}
