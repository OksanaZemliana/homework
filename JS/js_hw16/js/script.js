'use strict';
// Технические требования:
//
//     Написать функцию для подсчета n-го обобщенного числа Фибоначчи. Аргументами на вход будут три числа - F0, F1, n,
//     где F0, F1 - первые два числа последовательности (могут быть любыми целыми числами), n - порядковый номер
//     числа Фибоначчи, которое надо найти. Последовательнось будет строиться по следующему правилу
//     F2 = F0 + F1, F3 = F1 + F2 и так далее.
//     Считать с помощью модального окна браузера число, которое введет пользователь (n).
//     С помощью функции посчитать n-е число в обобщенной последовательности Фибоначчи и вывести его на экран.
//     Пользователь может ввести отрицательное число - результат надо посчитать по такому же правилу (F-1 = F-3 + F-2).

let userNumber;
while (!userNumber) {
    userNumber = +prompt('Enter your number');
}

function calcFibonacci(fibNumber) {
    if (fibNumber >= 0) {
        let fibNumber1 = 1;
        let fibNumber2 = 1;
        for (let index = 3; index <= fibNumber; index++) {
            let fibNumber3 = fibNumber1 + fibNumber2;
            fibNumber1 = fibNumber2;
            fibNumber2 = fibNumber3;
        }
        return fibNumber2;
    } else {
        let fibNumber1 = -1;
        let fibNumber2 = -1;
        for (let index = -3; index >= fibNumber; index--) {
            let fibNumber3 = fibNumber2 + fibNumber1;
            fibNumber2 = fibNumber1;
            fibNumber1 = fibNumber3;
        }
        return fibNumber1;
    }
}

alert('Fibonacci number is: ' + calcFibonacci(userNumber));