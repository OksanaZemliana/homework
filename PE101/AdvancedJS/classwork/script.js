// // 'use strict';
// class User {
//     constructor(name, age, password, secretQuestions, secretAnswer) {
//         this.name = name;
//         this.age = age;
//         this._password = password;
//         this._secretAnswer = secretAnswer;
//         this._secretQuestions = secretQuestions;
//     }
//
//     get password() {
//         const userSecretAnswer = prompt(this._secretQuestions);
//         if (userSecretAnswer === this._secretAnswer) {
//             return this._password;
//         } else {
//             return {
//                 status: 401,
//                 message: 'Access denied: wrong password',
//             }
//         }
//     }
//
//     set password(newPassword) {
//         const oldPassword = prompt('Enter your current password');
//         if (oldPassword === this._password) {
//             this._password = newPassword;
//             console.log(`Password was successfully changed. New password: ${this._password}`)
//             return {
//                 status: 200,
//                 message: 'success',
//             }
//         } else {
//             console.error('Access denied: wrong password');
//             return {
//                 status: 401,
//                 message: 'Access denied: wrong password',
//             }
//         }
//     }
// }
//
// const jhon = new User('Jhon', 31, '123123', 'Dog\'s name', 'Bob');
//
// console.log('jhon.password: ', jhon.password);
//
//
// jhon.password = '321321';
//
// setTimeout(() => {
//     jhon.password = '111111';
// }, 4000);
