'use strict';
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

class NoDateError extends Error {
    constructor(options) {
        super();
        this.name = "NoDateError";
        this.message = `Not exist in the object the property: '${options.toUpperCase()}'`;
    }
}

class ItemList {
    constructor({author, name, price}) {
        if (!author) {
            throw new NoDateError('author');
        }
        if (!name) {
            throw new NoDateError('name');
        }
        if (!price) {
            throw new NoDateError('price');
        }
        this.author = author;
        this.name = name;
        this.price = price;

    }

    render() {
        const li = document.createElement('li');
        li.innerText = `Author is: ${this.author}, name is: ${this.name}, price is: ${this.price}`;
        return li;
    }
}

class List {
    constructor(array, name) {
        this.array = array;
        this.name = name;
    }

    render() {
        const ul = document.createElement('ul');
        ul.textContent = this.name;

        this.array.forEach((el) => {
            try {
                ul.append(new ItemList(el).render())

            } catch (err) {
                    console.warn(err);
            }
        })
        return ul;
    }

}

const booksList = new List(books, "Books");
const root = document.getElementById('root');
root.append(booksList.render());



