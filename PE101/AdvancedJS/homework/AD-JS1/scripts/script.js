class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get name() {
        return this._name
    }

    set name(value) {
        if (typeof value === 'string' && value !== "") {
            this._name = value;
        }
    }

    get age() {
        return this._age
    }

    set age(value) {
        if (!isNaN(value) || value !== "" || value > 0) {
            this._age = value;
        }
    }

    set salary(value) {
        if (!isNaN(value) || value !== "" || value > 0) {
            this._salary = value;
        }
    }

    get salary() {
        return this._salary
    }


}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    set salary(value) {
        if (!isNaN(value) || value !== "" || value > 0) {
            this._salary = value;
        }
    }

    get salary() {
        return this._salary * 3
    }


}


const programmer = new Programmer('Artur', 40, 500, ['JS', 'C++']);
const programmer2 = new Programmer('Oksana', 25, 1000, ['JS', 'Java']);

console.log(programmer, programmer.salary);
console.log(programmer2, programmer2.salary);
