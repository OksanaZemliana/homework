'use strict';

const URL_BASE = 'https://ajax.test-danit.com/api/json/';
const root = document.querySelector('#root');
const templatePost = document.querySelector('#posts-list').content;
const list = templatePost.querySelector("ul").cloneNode();
root.append(list)


class RequestDates {
    constructor(url, entity = '') {
        this.url = url;
        this.entity = entity;

    }

    get() {
        return fetch(this.url + `${this.entity}`).then(response => response.json())
    }

    delete(data, id) {
        return fetch(this.url + `${this.entity}/${id}`, {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json",
            },
        })
            .then(({status}) => {
                if (status === 200) {
                    data.remove()
                }
            })
    }

    change(method, body, id = '', updateDates) {
        fetch(this.url + `${this.entity}/${id}`, {
            method: method,
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({body}),
        })
            .then((response) => response.json())
            .then(({status, body, error}) => {
                console.log(body)
                if (status === '200') {
                    updateDates(body)
                }
                if (status === 'error') {
                    alert(error)
                }
            })
    }
}

class Card {
    constructor(template, id) {
        this.template = template;
        this.id = id;
        this.listItem = this.template.querySelector("li").cloneNode();
        this.deleteButton = document.createElement('button');
        this.editButton = document.createElement('button');
        this.title = document.createElement('h3');
        this.text = document.createElement('p');

    }

    render({title, body, name, email}) {
        this.title.innerText = title;
        this.text.textContent = body;
        this.listItem.innerHTML = `
        <p>${name}     ${email}</p>
   
           `;

        this.deleteButton.innerText = 'Delete';
        this.editButton.innerText = 'Edit';
        this.listItem.append(this.title, this.text, this.deleteButton, this.editButton);
        return this.listItem;
    }

    delete(request) {
        this.listItem.dataset.id = this.id;
        this.deleteButton.addEventListener('click', () => {
            request.delete(this.listItem, this.id)
        })
    }

    edit(request) {
        this.listItem.dataset.id = this.id;
        this.title.setAttribute('contenteditable', 'true')
        this.text.setAttribute('contenteditable', 'true')
        this.editButton.addEventListener('click', () => {
            request.change('PUT', {title: this.title.innerText, body: this.text.innerText}, this.id, this.render)
        })
    }
}

const requestDatesPosts = new RequestDates(URL_BASE, 'posts');
const requestDatesUsers = new RequestDates(URL_BASE, 'users');

requestDatesUsers.get().then(users => {
    requestDatesPosts.get().then(posts => {
        posts.forEach(({userId, title, body, id}) => {
            const {name, email} = users.find(({id}) => id === userId)
            const card = new Card(templatePost, id);
            list.append(card.render({title, body, name, email}))
            card.delete(requestDatesPosts);
            card.edit(requestDatesPosts);
        })
    })
}).catch(err => console.log(err));
