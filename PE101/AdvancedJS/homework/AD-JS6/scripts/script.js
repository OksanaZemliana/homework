'use strict';
class User {
    constructor({country, timezone: continent, city, regionName}) {
        this.country = country;
        this.continent = continent;
        this.city = city;
        this.regionName = regionName;

    }

    render(selector) {
        document.querySelector(selector).innerHTML = `
        <div class="user-card">
				<p>Continent: ${this.continent}</p>
				<p>Country: ${this.country}</p>
				<p>City: ${this.city}</p>
				<p>Region: ${this.regionName}</p>
		</div>`
    }
}

const getIpAddress = async () => {
    const ip = await fetch('https://api.ipify.org/?format=json').then(res => res.json())
      return ip;
}

const getIpUser = async (ip) => {
      const userAddress = await fetch(`http://ip-api.com/json/${ip}`).then(res => res.json())
    return userAddress
}


const btn = document.querySelector('.header-btn');
        btn.addEventListener('click', async () => {
            try {
                const {ip} = await getIpAddress();
                const dataUserIp = await getIpUser(ip);
                console.log(dataUserIp)
                new User(dataUserIp).render('.container');
            }catch (err) {
                console.log(err)
            }
        })




