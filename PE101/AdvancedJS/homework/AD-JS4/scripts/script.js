"use strict";
const URL_BASE = "https://ajax.test-danit.com/api/swapi/films";
const root = document.querySelector("#root");
const listWars = document.createElement("ul");
listWars.className = 'list-wars';

const getData = () => fetch(URL_BASE).then(response => response.json());

const renderDate = (data, list, root) => {
    data.forEach(({episodeId, name, openingCrawl, characters}) => {
        const li = document.createElement("li");
        li.className = 'item-wars'
        li.insertAdjacentHTML('beforeend', `
    <h2>${name}</h2>
    <h3>Номер Епізоду:  ${episodeId}</h3>
    <p>Короткий опис:  ${openingCrawl}</p>  
    <h3>Actors:</h3>
    <div class='lds-dual-ring'></div>`)
        list.append(li);

        const charactersList = characters.map(character => fetch(character).then(res => res.json()));
        Promise.all(charactersList).then((response) => {
            response.forEach(({name}) => {
                document.querySelectorAll('.lds-dual-ring').forEach(el => el.remove())
                li.innerHTML += `${name}<br/>`;
            });
        });

    });
    root.append(list)
}
getData().then(data => renderDate(data, listWars, root)).catch(err=> console.log(err));
