'use strict';
//Теоретичні питання
// Опишіть своїми словами, що таке метод об'єкту
// Це функція , яка виконується в даному об'єкті

// Який тип даних може мати значення властивості об'єкта?
//Значення властивості може бути будь-якого типу, які є в JS

// Об'єкт це посилальний тип даних. Що означає це поняття?
// Це означає, що значенням є посилання на дані , а не самі дані

// Технічні вимоги:
//     Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в
// нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль
// результат виконання функції.
//     Необов'язкове завдання підвищеної складності
// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName()
// та setLastName(), які дозволять змінити дані властивості.

function createNewUser() {
    return {
        firstName: prompt('Enter your first name'),
        lastName: prompt('Enter your last name'),
        getLogin() {
            return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
        },
        // get firstName() {
        //     return this._firstName;
        // },
        // get lastName() {
        //     return this._lastName;
        // },
        // set firstName(value) {
        //     this._firstName = value;
        // },
        // set lastName(value) {
        //     this._lastName = value;
        // },
    }

}

const newUser = createNewUser();
console.log(newUser.getLogin());

let newFirstName;
let newLastName;
Object.defineProperties(newUser, {
    firstName : {
        get() {
            return newFirstName;

        },
        set (value) {
            newFirstName = value;
        },
    },
    lastName: {
        get() {
            return newLastName;
        },
        set(value) {
            newLastName = value;
        },
    }
});

newUser.firstName = 'Artem';
newUser.lastName = 'Rud';
console.log(newUser.firstName);
console.log(newUser.lastName);
console.log(newUser.getLogin());
console.log('hello');

