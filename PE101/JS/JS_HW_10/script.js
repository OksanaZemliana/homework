'use strict';


const deleteClass = (list, classDelete) => list.forEach(element => element.classList.remove(classDelete));

const changeTabs = () => {
    const tabListMenu = document.querySelectorAll('.tabs-title');
    const tabListContent = document.querySelectorAll('.content-item');
    tabListMenu.forEach(element => {
        element.addEventListener('click', () => {
            let currentTab = element;
            let tabId = currentTab.getAttribute('data-tab');
            let currentBtn = document.querySelector(tabId);
            if (!currentTab.classList.contains('active')) {
                deleteClass(tabListMenu, 'active');
                deleteClass(tabListContent, 'active');
                currentTab.classList.add('active');
                currentBtn.classList.add('active');
            }
        })
    })
}
changeTabs();
