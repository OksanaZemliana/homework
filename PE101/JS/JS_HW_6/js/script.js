'use strict';
//Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
//Для використання спеціальних символів ми їх екрануємо (маскуємо). Це і є екранування. Потрібно для роботи з спеціальними символами.

// Які засоби оголошення функцій ви знаєте?
//function declaration - function nameFunction(параметр) {інструкція}
//function expression - const nameFunction = function(параметр) {інструкція}
//arrow function expression - const nameFunction = (параметр) => {інструкція}

// Що таке hoisting, як він працює для змінних та функцій?
// Це процес при якому змінні і оголошення функцій переміщаються вверх своєї області видимості до виконання коду.
//Змінні можна ініціалізувати до їх оголошення, але не можна використовувати без ініціалізації. Функції можна визивати до її оголошення.

const formatDate = (date) => {
    let arr = date.split('.')
    return new Date(`${arr[1]}.${arr[0]}.${arr[2]}`);
};

function createNewUser() {
    return {
        firstName: prompt('Enter your first name'),
        lastName: prompt('Enter your last name'),
        birthday: formatDate(prompt('Enter your birthday', 'dd.mm.yyyy')),
        getLogin() {
            return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
        },
        getAge() {
            let monthNow = new Date().getMonth();
            let dateNow = new Date().getDate();
            let yearNow = new Date().getFullYear();
            if (monthNow === this.birthday.getMonth() && dateNow < this.birthday.getDate() || monthNow < this.birthday.getMonth()) {
                return yearNow - this.birthday.getFullYear() - 1;
            }
            return yearNow - this.birthday.getFullYear();
        },
        getPassword() {
            return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.getFullYear()}`;
        },

    }

}

const newUser = createNewUser();
console.log(newUser.getAge());
console.log(newUser.getPassword());