'use strict';
const images = document.querySelectorAll('.image-to-show');
const firstImage = images[0];
const lastImage = images[images.length - 1];
let timerId;
const showImages = () => {
    let currentElement = document.querySelector('.visible');
    if (currentElement !== lastImage) {
        currentElement.classList.remove('visible');
        currentElement.nextElementSibling.classList.add('visible')
    } else {
        currentElement.classList.remove('visible');
        firstImage.classList.add('visible');
    }
}
const btnStop = document.querySelector('.btn-stop');
const btnRestart = document.querySelector('.btn-restart');
btnStop.addEventListener('click', ()=>{
    clearInterval(timerId);
    btnRestart.disabled = false;
    btnStop.disabled = true;
})
btnRestart.addEventListener('click', ()=>{
    timerId = setInterval(showImages, 3000)
    btnRestart.disabled = true;
    btnStop.disabled = false;
})
btnRestart.click();
