'use strict';

const changeStyle = () => {
    const buttons = Array.from(document.querySelectorAll('.btn'));
    document.addEventListener('keydown', (event) => {
        buttons.forEach(element => {
            element.dataset.key === event.code
                ? element.classList.add('active')
                : element.classList.remove('active');
        });
    });
}
changeStyle();



