'use strict';
//## Теоретичні питання
// 1. Опишіть своїми словами що таке Document Object Model (DOM)
//  Це програмний інтерфейс доступу до елементів веб-сторінок, який
//  дозволяє читати і маніпулювати вмістом, структурою і стилями сторінки.

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//innerHTML проаналізує отриманий контент і інтерпритує теги.
// innerText буде читати все як текст

// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
//document.querySelectorAll(), document.querySelector(), document.getElementById(), getElementsByName(),
//getElementsByTagName(), getElementsByClassName()
//Найкращий спосіб є document.querySelectorAll(), querySelector тому що він є універсальним (може шукати по
// різним значенням (id , class, атрібут)) і отримує статичную колекцію.


//1) Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraphAll = Array.from(document.querySelectorAll('p'))
paragraphAll.forEach(value => value.style.background = '#ff0000');

//2) Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const optionsList = document.getElementById('optionsList');
console.log(optionsList);
console.log(optionsList.parentElement);
const optionsListChild = Array.from(optionsList.childNodes);
if (optionsList.hasChildNodes()) {
    optionsListChild.forEach(value =>
        console.log(`Name is : ${value.nodeName} type Node is : ${value.nodeType}`))
}

//3) Встановіть в якості контента елемента з класом testParagraph наступний параграф - <p>This is a paragraph<p/>
const testParagraph = document.querySelector('#testParagraph');
testParagraph.textContent = "This is a paragraph";

//4) Отримати елементи <li>, вкладені в елемент із класом main-header і вивести їх у консоль.
// Кожному з елементів присвоїти новий клас nav-item.
const mainHeaderList = Array.from(document.querySelectorAll('.main-header li'));
mainHeaderList.forEach(value => value.classList.add('nav-item'));
console.log(mainHeaderList);

//5) Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const sectionTitle = Array.from(document.querySelectorAll('.section-title'));
sectionTitle.forEach(value => value.classList.remove('section-title'));
console.log(sectionTitle);


