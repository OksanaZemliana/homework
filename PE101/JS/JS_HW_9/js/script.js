'use strict';
//## Теоретичні питання
//
// 1. Опишіть, як можна створити новий HTML тег на сторінці.
//Створити за допомогою метода createElement()
// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
//Перший параметр означає місце куди буде вставлений : beforebegin - вставити HTML перед елементом, afterbegin - на початок елемента
//beforeend - на кінець елемента, afterend - після елемента
// 3. Як можна видалити елемент зі сторінки?
// метод remove().

const getList = (array, elementOfDom = document.body) => {
    const ul = document.createElement('ul');
    const list = array.map(value => {
        const li = document.createElement('li');
        Array.isArray(value) ? getList(value, li) : li.textContent = value;
        return li;
    })
    ul.append(...list);
    elementOfDom.append(ul);

}

getList(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]);

const clearPage = () => {
    const divTimeBack = document.createElement('div');
    let seconds = 3;
    document.body.prepend(divTimeBack);
    divTimeBack.innerHTML = `Залишилось до очишення: ${seconds}`;
    setInterval(() => divTimeBack.innerHTML = `Залишилось до очишення: ${--seconds}`, 1000);
    setTimeout(() => document.body.remove(), 3000);
}
clearPage();