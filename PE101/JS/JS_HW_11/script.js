'use strict';

const submitForm = () => {
    const formPassword = document.querySelector('.password-form');
    const massageUser = document.querySelector('.massage-user');
    formPassword.addEventListener('click', (event) => {
        const target = event.target.closest('i');
        if (target) {
            const inputTarget = target.previousElementSibling;
            target.classList.toggle('fa-eye');
            target.classList.toggle('fa-eye-slash');
            if (target.classList.contains('fa-eye-slash')) {
                inputTarget.setAttribute('type', 'text')
            } else {
                inputTarget.setAttribute('type', 'password')
            }
        }
    })
    formPassword.addEventListener('submit', (event) => {
        event.preventDefault();
        const inputPsw = document.querySelector('#psw');
        const inputPswConfirm = document.querySelector('#psw-confirm');
        if (inputPsw.value !== inputPswConfirm.value || inputPsw.value === '' || inputPswConfirm.value === '') {
            massageUser.style.display = 'block'
        } else alert('You are welcome!');

    })

    formPassword.addEventListener('focus', (event) => {
        const target = event.target.closest("input");
           if (target && massageUser.textContent !== '')
               massageUser.style.display = 'none'
    }, {capture: true});

}
submitForm()
