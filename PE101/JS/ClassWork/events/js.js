'use strict';
// Написать скрипт, который создаст элемент button с текстом «Войти». При клике по
// кнопке выводить alert с сообщением: «Добро пожаловать!».
//
// Callback — это функция, которая срабатывает в ответ на событие. Совсем как в
// сервисом с функцией «перезвоните мне». Мы оставляем заявку «перезвоните мне» —
// это событие. Затем специалист на перезванивает (calls back).

// document.body.insertAdjacentHTML("afterbegin", '<button> Enter </button>');
// const btn = document.querySelector('button');
// btn.addEventListener("click", () => {
//     alert('Hello')
// });

// Улучшить скрипт из предыдущего задания. При наведении на кнопку указателем мыши,
//     выводить alert с сообщением: «При клике по кнопке вы войдёте в систему.».
// Сообщение должно выводиться один раз.
//
//     Условия:
//
// -   Решить задачу грамотно.
// btn.addEventListener('mouseenter', () => {
//         alert('При клике по кнопке вы войдёте в систему.')
//     },
//     {once: true});

//Создать элемент h1 с текстом «Добро пожаловать!». Под элементом h1 добавить
// элемент button c текстом «Раскрасить». При клике по кнопке менять цвет каждой
// буквы элемента h1 на случайный.
//
// /_ Дано _/
//
//     const PHRASE = 'Добро пожаловать!';
//
//     function getRandomColor() {
//         const r = Math.floor(Math.random() * 255);
//         const g = Math.floor(Math.random() * 255);
//         const b = Math.floor(Math.random() * 255);
//
//         return `rgb(${r}, ${g}, ${b})`;
//     }
// const title = document.createElement('h1');
// const phrase = 'Добро пожаловать!';
// title.textContent = phrase;
// document.body.append(title)
// const btn = document.createElement('button');
// btn.textContent = "Раскрасить";
// title.after(btn);
//     function getRandomColor() {
//         const r = Math.floor(Math.random() * 255);
//         const g = Math.floor(Math.random() * 255);
//         const b = Math.floor(Math.random() * 255);
//
//         return `rgb(${r}, ${g}, ${b})`;
//     }
//
//     btn.addEventListener('click', ()=> {
//         const fr = document.createDocumentFragment();
//         for (let i= 0; i < phrase.length; i++) {
//             const str = document.createElement('span');
//             str.textContent = phrase[i]
//             str.style.color = getRandomColor();
//             fr.append(str)
//         }
//         title.innerHTML = '';
//         title.append(fr)
//     })



//## TASK 1
//
// Рахувати кількість натискань на пробіл, ентер,
// та Backspace клавіші
// Відображати результат на сторінці
//
// ADVANCED: створити функцію, яка приймає тільки
// назву клавіші, натискання якої потрібно рахувати,
// а сам лічильник знаходиться в замиканні цієї функції
// (https://learn.javascript.ru/closure)
// id елемента, куди відображати результат має назву
// "KEY-counter"
//
// Наприклад виклик функції
// createCounter('Enter');
// реалізовує логіку підрахунку натискання клавіші Enter
// та відображає результат в enter-counter блок
//
// const keydown = () => {
//     let enter = 0;
//     let backspace = 0;
//     let space = 0;
//     const resultEnter = document.querySelector('#enter-counter');
//     const resultBackspace = document.querySelector('#backspace-counter');
//     const resultSpace = document.querySelector('#space-counter');
// document.addEventListener('keydown', (event)=> {
//
//     switch (event.code) {
//         case 'Enter' :
//             resultEnter.textContent = ++enter;
//             break;
//         case  'Backspace' :
//             resultBackspace.textContent = ++backspace;
//             break;
//         case 'Space' :
//             resultSpace.textContent = ++space;
//             break;
//     }
// })}
// keydown();

let menu = document.querySelector("#menu");
menu.addEventListener("click", function (event) {
    event.preventDefault();
    let active = document.querySelector(".active");
    if (active !== null) {
        document.querySelector(".active").classList.remove("active");
    }
    if (event.target.tagName === "A") {
        event.target.classList.add("active");
    }
});



