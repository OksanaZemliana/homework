'use strict';


//##TASK 1.1
// 	- Найти все элементы с классом text-block. Второму элементу добавить класс "www"
//
// const textBlock = document.querySelectorAll('.text-block');
// textBlock[1].classList.add('www')
// console.log(textBlock)
//
// // ##TASK 1.2
// // - Для всех элементов, у которых нет класса `www` и нет атрибута id сделать цвет текста равным f00
// // const textBlockNo = Array.from(document.querySelectorAll('p')).filter(value => !value.matches('.www , [id]'));
// const textBlockNo = document.querySelectorAll('p:not(.www , [id])')
// textBlockNo.forEach(value => value.style.color = '#ff0000')
//
// // ##TASK 1.3
// // - Для элементов с классом text-block задать цвет текста `blue`, нижжнюю границу "1px solid #000";
// textBlock.forEach(value => value.style.cssText = 'color: blue; border-bottom: 1px solid #000')
//
// // // ##TASK 1.4
// // // - вывести значения margin-block-start и margin-block-end как текст внутрь элементов
// // const getMargin = (element) => {
// //     for (let i = 0; i < element.length; i++){
// //         let a = window.getComputedStyle(element[i]).marginBlockEnd
// //         let b = window.getComputedStyle(element[i]).marginBlockStart
// //         element[i].textContent = a + b;
// //     }
// //
// // }
// // getMargin(textBlock)
// textBlock.forEach((item) => {
//     const {marginBlockStart, marginBlockEnd} = window.getComputedStyle(item);
//     item.textContent += `mbs: ${marginBlockStart}, mbe ${marginBlockEnd}`;
// })

// // ##TASK 1.5
// - Клонировать первый элемент, у которого есть класс `www`.
// Результат вывести в консоль.
//
// const newElement = document.querySelector('.www').outerHTML;
// // const newElement = document.querySelector('.www').cloneNode(true);
// console.log(newElement)
//
// // ##TASK 1.6
// // - После всех элементов, у которых нет класса `www` вставить элемент у которого есть класс `www`.

// document.body.querySelector(':not(.www)').parentElemet.innerHTML += newElement;

// wwwNo[wwwNo.length - 1].after(document.querySelector('.www'))
// console.log(document.querySelectorAll('p'))


// ##TASK 2
// Создать элемент `p`. По своему усмотрению задать стили (не меньше 4 параметров). Получить список всех стилей элемента
// (в формате "название -> значение"), которые имеют не пустое значение.
// Вывести список внутрь данного элемента `p`.
// `p` вывести внутрь элемента #root.
// const div = document.createElement('div');
// div.id = 'root';
// document.body.append(div);
//
// const p = document.createElement('p');
// p.style.cssText = 'color: green; font-weight: ; padding-left: 20px; background-color: yellow';
// div.append(p);
// let listStyle = getComputedStyle(p);
// for (const key in listStyle) {
//     if (listStyle[key]) {
//         p.textContent += `${key} - ${listStyle[key]}; `;
//     }
// }


//
// ##TASK 3.1
// - Создать с помощь createElement список(нумерованый либо маркированый) из 10 элементов. Внутрь каждого элемента, в
// качестве текста, вставить рандомное число от 10 до 20. Вывести список внутрь #root.
const ul = document.createElement('ul');
for (let i = 1; i <= 10; i++) {
    let li = document.createElement('li');
    li.innerText = Math.floor(Math.random() * 10) + 10;
    ul.append(li)
}
document.body.append(ul);

// ##TASK 3.2
// - Выполнить задачу из предыдущего задания, используя DocumentFragment.
// const ulTwo = document.createElement('ul');
// const frag = document.createDocumentFragment();
// for (let i = 1; i <= 10; i++) {
//     let li = document.createElement('li');
//     li.innerText = Math.floor(Math.random() * 10) + 10;
//     frag.append(li);
// }
//
// ulTwo.append(frag);
// div.append(ulTwo)


// ##TASK 3.3
// - Ниже вывести второй список с классом `.sorted`, в котором значения из первого будут отсортированы по убыванию. Использовать DocumentFragment.
// let sort = new DocumentFragment();
// sort.className = 'sorted';
// const sortList = (original, clone) => {
//     let ul = original.cloneNode(true);
//     let list = [];
//     let newUL = document.createElement('ul')
//     ul.childNodes.forEach(item => {list.push(item.textContent)});
//     list.sort((a1, b1) => b1 - a1);
//     list.forEach(item => {
//         let newLi = document.createElement('li');
//         newLi.textContent = item;
//         newUL.append(newLi)
//     })
//     clone.append(newUL)
// }
// sortList(ul, sort);
// p.before(sort);
const sorted = document.createElement('ul');
sorted.className = 'sorted';
const item = [...ul.children];
const beSortedItem = item.map((el) => el.cloneNode(true));
beSortedItem.sort((curr, next) => next.textContent - curr.textContent)
sorted.append(...beSortedItem);
document.body.append(sorted);
// ##TASK 3.4
// - написать функцию по вычислению среднего арифметического чисел. Вывести значение среднего арифметического как первый элемент списка `.sorted`.

//     const listLi = ul.querySelectorAll('li');
//
//     let array = [];
//     listLi.forEach(item => {
//        array.push(+item.textContent)
//            });
// ul.prepend((array.reduce((a, b) => a + b, 0))/ array.length);

const average = beSortedItem.reduce((sum,{textContent: num}) => sum + +num, 0) / beSortedItem.length;
sorted.innerHTML = `<li>${average}<li>${sorted.innerHTML}`;
// ##TASK 3.5
// - на базе списка `.sorted` создать третий список с классом `.filtered`, который будет содержать только те значения, которые больше 15. Использовать DocumentFragment.
const filterItems = beSortedItem.map((el) => el.cloneNode(true)).filter(el => +el.textContent > 15);
let ulFilter = document.createElement('ul');
ulFilter.className = 'filtered';
ulFilter.append(...filterItems)
document.body.append(ulFilter)
// ##TASK 4
// В HTML дано список из 5 элементов с произвольным текстовым содержимым.
// Пользователь вводит число (порядковый номер элемента) в диапазоне от 1 до 5 (сделать проверку: если пользователь ввел число вне диапазона попросить у него ввести еще раз).
//
// 	Вывести под списком в параграфе с классом `siblingContent` текстовое содержимое следующего и предыдущего элементов.
// 	  <ul>
// 	    <li>1</li>
// 	    <li>2</li>
// 	    <li>3</li>
// 	    <li>4</li>
// 	    <li>5</li>
// 	  </ul>
//  <!-- при вводе числа 3 выводим <p>'2' и '4'</p> -->
// let elementIndex;
// do {
//     elementIndex = prompt('enter the number');
// } while (elementIndex > 5 || elementIndex < 1 || elementIndex % 1 !==0);
// const paragraf = document.createElement('p');
// paragraf.className = 'siblingContent';
// const array = [...document.querySelectorAll('li')].map((item) => item.textContent);
// paragraf.textContent = (array[elementIndex] || + '') + (array[elementIndex-2] || '');
// document.querySelector('ul').after(paragraf)
// ##TASK 5.1
// 	Напишите функцию, которая удаляет элемент из DOM. Порядковый номер элемента получить у пользоватьеля.
//  	<div>1 element</div>
// 	<div>2 element</div>
// 	<div>3 element</div>
// 	<div>4 element</div>
// 	<div>5 element</div>
// 	<div>6 element</div>
// 	<div>7 element</div>
// 	<div>8 element</div>
const deleteElement= () => {
    let number;
    do {
        number = +prompt('enter number > 0');
    } while (number < 1 || isNaN(number));
    const elDel = document.querySelector('div:nth-child(number)');
    elDel.remove()

}
deleteElement();
// ##TASK 5.2
// Напишите функцию insertAfter(elem, refElem), которая добавит elem после узла refElem. Порядковый номер refElem получить у пользоватьеля.
// 	const elem = document.createElement('div');
//  	elem.innerHTML = '<b>Новый элемент</b>';
//
//
// ##TASK 6
//  Напишите функцию removeChildren, которая удаляет всех потомков элемента.
// 	<table>
//   		<tr>
//     		<td>Это</td><td>Все</td><td>Элементы DOM</td>
//   		</tr>
// 	</table>
// 	<ol>
//  		<li>Вася</li>
//   		<li>Петя</li>
//   		<li>Маша</li>
//   		<li>Даша</li>
// 	</ol>
//
//
// ##TASK 7
// Напишите интерфейс для создания списка.
//
// Для каждого пункта:
//
// Запрашивайте содержимое пункта у пользователя с помощью prompt. Создавайте пункт и добавляйте его к UL.
// Процесс прерывается, когда пользователь нажимает ESC.
// Все элементы должны создаваться динамически.
// Если посетитель вводит теги — в списке они показываются как обычный текст.
//
//
// ##TASK 8
// Напишите функцию, которая создаёт вложенный список UL/LI (дерево) из объекта.
// var data = {
//   "Рыбы":{
//     "Форель":{},
//     "Щука":{}
//   },
//
//   "Деревья":{
//     "Хвойные":{
//       "Лиственница":{},
//       "Ель":{}
//     },
//     "Цветковые":{
//       "Берёза":{},
//       "Тополь":{}
//     }
//   }
// };
//
//
// ##TASK 9
// Напишите функцию, которая умеет генерировать календарь для заданной пары (месяц, год).
//
// Календарь должен быть таблицей, где каждый день — это TD. У таблицы должен быть заголовок с названиями дней недели, каждый день — TH.
//
// Синтаксис: createCalendar(id, year, month).
//
// 	Такой вызов должен генерировать текст для календаря месяца month в году year, а затем помещать его внутрь элемента с указанным id.
//
// 	<style>
// 	table {
// 	  border-collapse: collapse;
// 	}
//
// 	td, th {
// 	  border: 1px solid black;
// 	  padding: 3px;
// 	  text-align: center;
// 	}
//
// 	th {
// 	  font-weight: bold;
// 	  background-color: #E6E6E6;
// 	}
// 	</style>
//
// function createCalendar(id, year, month) {
//   var elem = document.getElementById(id)
//
//   // ... ваш код, который генерирует в elem календарь
// }
//
// createCalendar('calendar', 2011, 1)