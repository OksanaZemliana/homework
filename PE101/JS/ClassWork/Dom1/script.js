'use strict'

// Получить элемент с классом .remove.
//     Удалить его из разметки.
//
//
//     Получить элемент с классом .bigger.
//     Заменить ему Project-x-класс .bigger на Project-x-класс .active.
//
//
//     Условия:
//
//
// Вторую часть задания решить в двух вариантах: в одну строку и в две
// строки.
// const el = document.querySelectorAll('.remove');
// el.remove();
//На экране указан список товаров с указанием названия и количества на складе.
// Найти товары, которые закончились и:
// Изменить 0 на «закончился»;
// Изменить цвет текста на красный;
// Изменить жирность текста на 600.
// Требования:
// Цвет элемента изменить посредством модификации атрибута style.
// const itemOffStock =document.querySelectorAll(".store li");
// itemOffStock.forEach(item=>{
//     item.textContent.split(": ").forEach(val=>{
//         if(+val===0){
//             item.innerHTML=item.textContent.replace("0","закончился");
//             item.style.cssText ="color: red; font-weight: 600";
//         }
//     })
//
// })
//
// console.log(itemOffStock);

const itemOffStock =document.querySelectorAll(".store li");
itemOffStock.forEach(item=>{
    if(+item.textContent.split(": ")[1] === 0){

            item.textContent=item.textContent.replace("0","закончился");
            item.style.cssText ="color: red; font-weight: 600";
        }


})

console.log(itemOffStock);
//     -   Задание 4.
// -
// -   Получить элемент с классом .list-item.
// -   Отобрать элемент с контентом: «Card 5».
//     -   Заменить текстовое содержимое этого элемента на ссылку, указанную в секции
//     «дано».
//     -   Сделать это так, чтобы новый элемент в разметке не был создан.
// -   Затем отобрать элемент с контентом: «Card 6».
// -   Заменить содержимое этого элемента на такую-же ссылку.
// -   Сделать это так, чтобы в разметке был создан новый элемент.
// -   Условия:
// -   -   Обязательно использовать метод для перебора;
// -   -   Объяснить разницу между типом коллекций: Array и NodeList. \*/
//
const targetElement = '<a href="https://www.google.com" target="_blank" rel="noreferrer noopener">Google it!</a>';

 // const listItem = document.querySelector('.list-item');

const getText = (selector, itemFind) => {
    return Array.from(document.querySelectorAll(selector)).filter(value => value.textContent.includes(itemFind));
}
const listItemFife = getText('.list-item', 'Card 5')
listItemFife.forEach(value => value.textContent = targetElement)
const listItemSix = getText('.list-item', 'Card 6')
listItemSix.forEach(value => value.innerHTML = targetElement)