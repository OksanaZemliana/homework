'use strict';

// Вывести на экран уведомление с текстом "Hello! This is alert" при помощи
// модального окна alert.
//alert("Hello! This is alert");


//Вывести на экран модальное окно prompt с сообщением "Enter the number".
// Результат выполнения модального окна записать в переменную, значение которой
// вывести в консоль.


//При помощи модального окна prompt получить от пользователя два числа. Вывести в
// консоль сумму, разницу, произведение, результат деления и остаток от деления их
// друг на друга.

//Записать в переменную '123', вывести в консоль typeof этой переменной.
// Преобразовать эту переменную в численный тип при помощи parseInt(),
// parseFloat(), унарный плюс + После этого повторно вывести в консоль typeof этой
// переменной.
// const number = '123';
// console.log(typeof number);
//
// console.log(typeof parseFloat(number));
// console.log(typeof parseInt(number));
// console.log(typeof +(number));


//Объяснить поведение каждое операции.
// let x, y, z;
// x = 6;
// y = 15;
// z = 4;
//
// x = 6;
// y = 15;
// z = 4;
// console.log((y /= x + (5 % z)));
// x = 6;

// z = 4;
// let s = 6;
//
// console.log(!!"false" == !!"true");
// console.log("true" == true);
// console.log("true" === true);
// console.log(NaN == 1);
// console.log(NaN == NaN);
// console.log(NaN === NaN);
// console.log(0 == "0");
// console.log(0 === "0");
// const first = false;
// const second = true;
// console.log(first == 0);
// console.log(second === 1);
//
// Пользователь вводит в модальное окно любое число.
//     В консоль вывести сообщение: Если число чётное, вывести в консоль сообщение
// «Ваше число чётное.»;
// Если число не чётное, вывести в консоль сообщение «Ваше число не чётное.»;
// Если пользователь ввёл не число, вывести новое модальное окно с сообщением
// «Необходимо ввести число!».
// Если пользователь во второй раз ввёл не число, вывести сообщение: «⛔️ Ошибка!
//     Вы ввели не число.
//
// let userNumber = +prompt('Enter your number');
// if (isNaN(userNumber) || userNumber === 0 || userNumber === null) {
//     alert('«Необходимо ввести число!»');
//     userNumber = +prompt('Enter number again');
//     if (isNaN(userNumber) || userNumber === 0) {
//         alert('⛔️ Ошибка! Вы ввели не число.')
//     } else if (userNumber % 2 === 0) {
//         console.log('«Ваше число чётное.»');
//     } else {
//         console.log('«Ваше число не чётное.»');
//     }
// } else {
//     if (userNumber % 2 === 0) {
//         console.log('«Ваше число чётное.»');
//     } else {
//         console.log('«Ваше число не чётное.»');
//
//     }
//
// }
// ;

//Написать программу, которая будет приветствовать пользователя. Сперва
// пользователь вводит своё имя, после чего программа выводит в консоль сообщение с
// учётом его должности.
// Список должностей: Mike — CEO; Jane — CTO; Walter — программист: Oliver —
// менеджер; John — уборщик.
// Если введёно не известное программе имя — вывести в консоль сообщение
// «Пользователь не найден.».
// Выполнить задачу в двух вариантах:
// используя конструкцию if/else if/else;
// используя конструкцию switch.

// let userName = prompt('Enter your name');
// if (userName === "Mike") {
//     console.log('Your position is CEO');
// } else if (userName === "Jane") {
//     console.log('Your position is CTO');
// } else if (userName === "Walter") {
//     console.log('Your position is programer');
// }  else if (userName === "Oliver") {
//     console.log('Your position is менеджер');
// }  else if (userName === "John") {
//     console.log('Your position is уборщик');
// } else {
//     console.log('Пользователь не найден');
// }
// switch (userName) {
//     case 'Mike' :
//         console.log('Your position is CEO');
//         break;
//     case 'Jane' :
//         console.log('Your position is CTO');
//         break;
//     case 'Walter' :
//         console.log('Your position is programer');
//         break;
//     case 'Oliver' :
//         console.log('Your position is manager');
//         break;
//     case 'John' :
//         console.log('Your position is CTO');
//         break;
//     default:
//             console.log('Пользователь не найден');
// }


//Пользователь вводит 3 числа. Вывести в консоль сообщение с максимальным числом
// из введённых.
// Если одно из введённых пользователем чисел не является числом, вывести
// сообщение: «⛔️ Ошибка! Одно из введённых чисел не является числом.».
// Условия: объектом Math пользоваться нельзя.
// const firstNumber = +prompt("Enter first number");
// const secondNumber = +prompt("Enter second number");
// const thirdNumber = +prompt("Enter third number");
// if (isNaN(firstNumber) || isNaN(secondNumber) || isNaN(thirdNumber)) {
//     console.log('«⛔️ Ошибка! Одно из введённых чисел не является числом.»');
// } else if (firstNumber > secondNumber) {
//     console.log(firstNumber);
// } else if (secondNumber > thirdNumber ) {
//     console.log(secondNumber);
// } else {
//     console.log(thirdNumber);
// }
// //


// Напишите программу «Кофейная машина».
// Программа принимает монеты и готовит напитки: Кофе за 25 монет; Капучино за 50
// монет; Чай за 10 монет.
//     Чтобы программа узнала что делать, она должна знать: Сколько монет пользователь
// внёс; Какой он желает напиток.
//     В зависимости от того, какой напиток выбрал пользователь, программа должна
// вычислить сдачу и вывести сообщение в конс.оль: «Ваш «НАЗВАНИЕ НАПИТКА» готов.
// //     Возьмите сдачу: «СУММА СДАЧИ»."
// Если пользователь ввёл сумму без сдачи — вывести сообщение: «Ваш «НАЗВАНИЕ
// // НАПИТКА» готов. Спасибо за сумму без сдачи! :)"
// const coinValue = +prompt('Enter your coins');
// const nameOfDrink = prompt('Enter drink');
// switch (nameOfDrink) {
//     case "кофе" :
//         if (coinValue === 25) {
//             alert(`Ваш ${nameOfDrink} готов. Спасибо за сумму без сдачи!`);
//         } else if (coinValue > 25) {
//             alert(`Ваш ${nameOfDrink} готов. Возьмите сдачу: ${coinValue - 25}`);
//         }
//         break;
//     case "капучино" :
//         if (coinValue === 50) {
//             alert(`Ваш ${nameOfDrink} готов. Спасибо за сумму без сдачи!`);
//         } else if (coinValue > 50) {
//             alert(`Ваш ${nameOfDrink} готов. Возьмите сдачу: ${coinValue - 50}`);
//         }
//         break;
//     case "чай" :
//         if (coinValue === 10) {
//             alert(`Ваш ${nameOfDrink} готов. Спасибо за сумму без сдачи!`);
//         } else if (coinValue > 10) {
//             alert(`Ваш ${nameOfDrink} готов. Возьмите сдачу: ${coinValue - 10}`);
//         }
//         break;
//     default :
//         alert("Напиток не найден заберите Ваши " + coinValue);
// }
// const coinValue = +prompt('Enter your coins');
// let nameOfDrink = prompt('Enter drink');
// switch (nameOfDrink) {
//     case "кофе" :
//
//         break;
//     case "капучино" :
//         break;
//     case "чай" :
//         break;
//     default :
//         alert("Напиток не найден заберите Ваши " + coinValue);
// }
// if (coinValue === 50 || coinValue === 25 || coinValue === 10) {
//     alert(`Ваш ${nameOfDrink} готов. Спасибо за сумму без сдачи!`);
// } else if (coinValue > 50 ) {
//     alert(`Ваш ${coffeDrink} готов. Возьмите сдачу: ${coinValue - 50}`);
//
// } else if (coinValue > 25 ) {
//     alert(`Ваш ${nameOfDrink} готов. Возьмите сдачу: ${coinValue - 25}`);
// }  else if (coinValue > 10 ) {
//     alert(`Ваш ${nameOfDrink} готов. Возьмите сдачу: ${coinValue - 10}`);
// }
// else {
//     alert(`Недостаточно денег`);
// }
//

// Задание 1.
//
//
// Написать функцию-сумматор.
//     Функция обладает двумя числовыми параметрами, и возвращает результат их
// сложения. */
// const sum = (a,b) => a+b;
// console.log(sum(2,3));


// /**
//
//  Задание 2.
//
//
//  Написать функцию, которая определяет количество переданных аргументов, и
//  возвращает их количество. */
// function getArgumenstCount() {
//     console.log(arguments.length);
// }
//
// getArgumenstCount(1,2,3,5);

//
//  Задание 3.
//  Написать функцию-счётчик count.
//  Функцию обладает будет двумя параметрами:
//  Первый — число, с которого необходимо начать счёт;
//  Второй — число, которым необходимо закончить счёт.
//  Если число, с которого начинается счёт больше, чем число,
//  которым он заканчивается, вывести сообщение:
//  «⛔️ Ошибка! Счёт невозможен.»
//  Если оба этих числа одинаковые, вывести сообщение:
//  «⛔️ Ошибка! Нечего считать.»
//  В начале счёта необходимо вывести сообщение:
//  «🏁 Отсчёт начат.».
//  Каждый «шаг» счёта необходимо выводить в консоль.
//  После прохождения последнего «шага» необходимо вывести сообщение:
//  «✅ Отсчёт завершен.».
// function count(a, b) {
//     if (!isNaN(a) && !isNaN(b) && a && b) {
//         if (a > b) {
//             console.log('Ошибка! Счёт невозможен.')
//         } else if (a === b) {
//             console.log('Ошибка! Нечего считать.')
//         } else {
//             console.log('Отсчёт начат.');
//             for (let i = a; i <= b; i++) {
//                 console.log(i);
//             }
//             console.log('Отсчёт завершен.')
//
//         }
//     } else {
//         console.log('Ошибка! Счёт невозможен.')
//     }
// }
// count(9, 10);
//

//
//  Задание 4.
//  Написать улучшенную функцию-счётчик countAdvanced.
//  Функцию-счётчик из предыдущего задания расширить дополнительным функционалом:
//  Добавить ей третий параметр, и выводить в консоль только числа, кратные
//  значению из этого параметра;
//  Генерировать ошибку, если функция была вызвана не с тремя аргументами;
//  Генерировать ошибку, если любой из аргументов не является допустимым
//  числом. */
// function count(a, b, c) {
//     if (arguments.length < 3) {
//         return;
//     }
//     if (!isNaN(a) && !isNaN(b) && a && b) {
//         if (a > b) {
//             console.log('Ошибка! Счёт невозможен.')
//             return;
//         }  if (a === b) {
//             console.log('Ошибка! Нечего считать.')
//             return;
//         }
//             console.log('Отсчёт начат.');
//             for (let i = a; i <= b; i++) {
//                 if (i % c === 0) {
//                     console.log(i);
//                 }
//             }
//
//             console.log('Отсчёт завершен.')
//
//         }
//
//
// else
// {
//     console.log('Ошибка! Счёт невозможен.')
//
// }
//
// }
// count(9, 10, 2);

//
//  Задание 5.
//  Написать функцию-сумматор всех своих параметров.
//  Функция принимает произвольное количество параметров.
//  Однако каждый из них обязательно должен быть числом.
//  Генерировать ошибку, если:
//  Хоть один из параметров не является допустимым числом (в ошибке указать
//  порядковый номер аргумента);
//  Если функция была вызвана менее, чем с двумя аргументами.

// function sumArguments() {
//     if (arguments.length < 2) {
//         return 'less then 2 parametrs';
//     }
//
//     let sum = 0;
//     for (let i = 0; i < arguments.length; i++) {
//         if (isNaN(arguments[i]) || typeof arguments[i] !== 'number') {
//             return `Not a number N argument is ` + (i);
//         }
//         sum += arguments[i];
//     }
//     return sum;
// }
//
// console.log(sumArguments(1, undefined, 3));
// //
// Задание 1.
// Написать функцию-счётчик increment.
// Первый вызов функции должен вернуть 0. Каждый новый вызов функции должен
// возвращать число, на 1 больше, чем предыдущее.
// Продвинутая сложность:
//
// На техническом языке подробно объяснить механизм решения.
// let count = 0;
//
// function makeIncrement() {
//     return count++;
// }
//
// console.log(makeIncrement());
// console.log(makeIncrement());

// Задание 2.
// Написать функцию createIncrement, которая будет возвращать другую функцию.
// Первый вызов этой другой функции должен вернуть 0. Каждый новый вызов этой
// другой функции должен возвращать число, на 1 больше, чем предыдущее.
// Каждый новый вызов родительской функции createIncrement будет возвращать новую
// «дочернюю» функцию-счётчик. Каждая новая дочерняя функция должна начинать отсчёт
// с 0.
// Условия:
// Использовать замыкания;
// Объявлять переменные на глобальном уровне модуля запрещено.
// Продвинутая сложность:
// На техническом языке подробно объяснить механизм решения.


// function makeIncrement() {
//     let count = 0;
//     return function () {
//         return count++;
//     }
//
// }
// let increment1 = makeIncrement();
// console.log(increment1());
// console.log(increment1());
// let increment2 = makeIncrement();
// console.log(increment2());
// console.log(increment2());
// console.log(increment2());

//
// Задание 3.
// Написать функцию-логгер log, которая выводит в консоль сообщение указанное
// количество раз.
// Функция обладает двумя параметрами:
//
// Первый — строковый тип, сообщение для вывода;
// Второй — числовой тип, количество выводов сообщения.
//
// Задать значения по-умолчанию для обеих параметров:
//
// Для первого — «Внимание! Сообщение не указано.»;
// Для второго — 1;
//
// Если первый аргумент(сообщение) не передан - ПО УМОЛЧАНИЮ присвоить этому
// аргументу - "Empty message" Если второй аргумент(количество раз) не передан - ПО
// УМОЛЧАНИЮ присвоить этому аргументу значение 1.

// const log = (message = 'Внимание! Сообщение не указано.»', count = 1) => {
//     for (let i=0; i <= count; i++) {
//         console.log(message);
//     }
// }
// log('bbbb', 5);
// Задание 4.
// Написать калькулятор на функциях.
// Программа должна выполнять четыре математические операции:
//
// Сложение (add);
// Вычитание (subtract);
// Умножение (multiply);
// Деление (divide).
//
// Каждую математическую операцию должна выполнять отдельная функция. Каждая такая
// функция обладает двумя параметрами-операндами, и возвращает результат нужной
// операции над ними.
// Эти вспомогательный функции использует главная функция calculate, которая
// обладает тремя параметрами:
//
// Первый — числовой тип, первый операнд;
// Второй — числовой тип, второй операнд;
// Третий — функцию, ссылка на ранее созданную вспомогательную функцию.
//
// Условия:
//
// Никаких проверок типов данных совершать не нужно;
// Обязательно использовать паттерн «callback».
// const add = (a, b) => {
//     return a + b;
// }
// const subtract = (a, b) => {
//     return a-b;
//
// }
// const multiply = (a, b) => {
//     return a * b;
//
// }
// const divide = (a, b) => a / b;
// function calculate (a, b, callback){
//  return callback(a, b)
// }
//
// console.log(calculate(7, 9, add))
//
// Задача 5
// Написать функцию, которая выводит в консоль таблицу умножения от 1 до числа
// введнного пользователем. Пример:
// '1 x 1 = 1'
// '1 x 2 = 2'
// '...'
// '9 x 9 = 81'
// '9 x 10 = 90'

// const makeMultiply = () => {
//     let numberUser = +prompt('number?');
//   for ( let i = 1 ; i <= numberUser ; i++) {
//       for (let a = 1; a < 10; a++)
//       console.log(`${i} * ${a} = ${i * a}`);
//   }
// }
// makeMultiply();
// Задача 6
// Напишите функцию, которая принимает произвольное колличество параметров и
// возвращает их в виде строки. Пример: stringify(1, 2, "a", null, 0, false) //
// вернет "1-2-a-null-0-false"
// function strArgument() {
//     let a = '';
//     for (let i = 0; i < arguments.length; i++) {
//         a += `-${arguments[i]}`;
//
//     }
//     return a;
//
// }
//
// console.log(strArgument(1, 2, "a", null, 0, false))
// Задача 7
// Напишите функцию, которая реализует следующий функционал
// Дана строка из 6-ти цифр. Проверьте, что сумма первых трех цифр равняется сумме
// вторых трех цифр. Если это так - выведите 'да', в противном случае выведите
// 'нет'.
// const checkSum = (a) => {
//  if ((a[0] + a[1] +a[2]) === (a[3] + a[4] +a[5])) {
//      return 'Yes'
//  } else return 'No'
// }
// console.log(checkSum('123143'));

//Написать функцию, которая определяет тип данных. Функция имеет два параметра:
// 
// данные любого типа
// тип данных для сравнения
// 
// Если переданный тип данных (второй параметр) и тип данных первого параметра
// совпадают, вернуть true, иначе вернуть false. Решение должно быть оптимальным,
// код максимально коротким.
// const check = (a, b) => typeof a === b;
// console.log(check('dfdf', 'string'))

// TASK 2
// В коде пропсианы данные константами: userName (имя пользователя), password
// (набор любых символов без пробелов). Значения придумать самостоятельно.
// Написать функцию, которая имитируеlo авторизацию на сайте.
// Спросить у пользователя его логин и пароль. Если пользователь ввеcheл данные
// (значения, которые пришли от пользователя не пустые) начинать проверку, если
// нет - вывети alert с текстом "Enter data".
// Дополнительные проверки:
// 
// имя пользователя должно быть не меньше 4 символов
// пароль не меньше 8 символов.
// 
// Функция дожна проверять, совпадают ли значения, введенные пользователем и
// значения, которые хранятся в коде. Если значения равны, вывести в alert текст:
// User ${userName}, your access confirmed, иначе вывести текст "Access denied".

// const user = 'Oksana';
// const password = 'oksanazemlyana';
//
// function checkValue(user, password) {
//     let userName = prompt('Enter your name');
//     let userPassword = prompt('Enter your password');
//     if (userName === null || userName === '' || userPassword === null || userPassword === '') {
//         alert('Enter data');
//         return;
//     }
//     if (userName.length < 4 || userPassword.length < 8) {
//         alert('not your date');
//         return;
//     } if (userName !== user || userPassword !== password) {
//         alert('Access denied')
//         return;
//     } alert(`User ${userName} your access confirmed`)
// }
// checkValue(user, password);
// TASK 3
// Написать функцию, которая собирает переданные значения в строку.
// Функция принимает произвольное количество аргументов и попарно преобразует в
// строку.
// Пример работы функции: Вызов с такими параметрами
// fn("name", "Uasya", "age", 33, "gender", "Male") должен вернуть строку
// name: Uasya, age: 33, gender: Male.
// Проверить, чтобы аргументы не были "ошибочными" значениями (null, "", undefined,
// NaN)
// function makeString () {
//     let text = '';
//         for ( let i = 0; i < arguments.length; i+=2){
//          if (arguments[i] && arguments[i+1]) {
//              text += !text ? '' : ', ';
//              text += arguments[i] + ': ' + arguments[i+1];
//
//          }
//
//     }return text;
// }
//
// console.log(makeString("name", "Uasya", "age", 33, "gender", "Male"))

// TASK 4
// Напишите функцию, которая принимает произвольное колличество параметров. Первым
// параметром передается тип данных. Функция должна "отфильтровать" параметры и
// вернуть те, тип данных которых не совпадает с первым параметром. Значения
// возвращаются в виде строки
// Пример: stringify("boolean", 1, 2, "a", null, 0, false) вернет
// "1-2-a-null-0"
// function stringify() {
//     let text = '';
//     for (let i = 1; i < arguments.length; i++) {
//         if (arguments[0] !== typeof arguments[i]) {
//             text += !text ? '' : '-';
//             text += arguments[i];
//         }
//     }
//     return text;
// }
//
// console.log(stringify("boolean", 1, 2, "a", null, 0, false));con

// const askUserInfo = (massage) => {
//     let userInfo;
//     do {
//         userInfo = prompt(massage);
//     } while (!userInfo);
//     return userInfo;
// }
// const getAge = () => {
//     let age;
//     do {
//         age = +prompt('Enter your Age');
//     } while (isNaN(age) || !age && age > 15 || age > 100);
// return age;
// }
//
// const haveChild = () => confirm('do you have child?');
//
// const user = {
//     firstName : askUserInfo('Enter your first name'),
//     lastName : askUserInfo('Enter your last name'),
//     age : getAge(),
//     children : haveChild(),
//     eyesColor : askUserInfo('What color is your eyes?'),
// }
//
// for (let  key in user) {
//     console.log(`Свойство ${key}: ${user[key]}`);
// }
//
// const isUserJobValid = (job) => job === 'developer' || job === "QA" || job === "designer" || job === "PM";
//
// const createUser = (userName = null, lastName = null, job = null) => {
//
//     return {
//         userName,
//         lastName,
//         _job: isUserJobValid(job) ? job : null,
//         get fullName() {
//             return `${this.userName} ${this.lastName}`
//         },
//
//         get job() {
//             return this._job;
//         },
//
//         set job(newValue) {
//             if (newValue === 'developer' || newValue === "QA" || newValue === "designer" || newValue === "PM") {
//                 this._job = newValue;
//             }
//         }
//     }
// }
//
//
// let lera = createUser('lera', 'vitv', 'physician');
// let john = createUser('john', 'vitv', 'designer');
//

//
// Задание 1.
// Создать объект пользователя, который обладает тремя свойствами:
//
//
//     Имя;
//
//
//
//
// Фамилия;

// Профессия.
//     А также одним методом sayHi, который выводит в консоль сообщение 'Привет.'.
// const user = {
//     name : 'Oksana',
//     lastName : 'Zemlyana',
//     job : 'PM',
//     sayHi(){
//         console.log(`Hi ${this.name} ${this.lastName}`);
//     }
// }
// user.sayHi();
//
//  Задание 2.
//  Расширить функционал объекта из предыдущего задания:
//  Метод sayHi должен вывести сообщение: «Привет. Меня зовут ИМЯ ФАМИЛИЯ.»;
//  Добавить метод, который меняет значение указанного свойства объекта.
//  Продвинутая сложность:
//  Метод должен быть «умным» — он генерирует ошибку при совершении попытки
//  смены значения несуществующего в объекте свойства. */
// const user = {
//     name : 'Oksana',
//     lastName : 'Zemlyana',
//     job : 'PM',
//     sayHi(){
//         console.log(`Hi ${this.name} ${this.lastName}`);
//     },
//     changeProperty(property, value){
//         if (property in user) {
//             //Object.hasOwn(this, property)
//             this[property] = value;
//         }
//
//         }
//
// }
// user.changeProperty('name', 'Job');
// user.sayHi();

//  Задание 3.
//  Расширить функционал объекта из предыдущего задания:
//  Добавить метод, который добавляет объекту новое свойство с указанным
//  значением.
//  Продвинутая сложность:
//  Метод должен быть «умным» — он генерирует ошибку при создании нового
//  свойства
//  свойство с таким именем уже существует. */
//
// const user = {
//     name: 'Oksana',
//     lastName: 'Zemlyana',
//     job: 'PM',
//     sayHi() {
//         console.log(`Hi ${this.name} ${this.lastName}`);
//     },
//     changeProperty(property, value) {
//         if (property in user) {
//             //Object.hasOwn(this, property)
//             this[property] = value;
//         }
//
//     },
//     addProperty(property, value) {
//         if (property in user) {
//             console.log('свойство с таким именем уже существует')
//         } else return this[property] = value;
//     }
//
// }
// user.addProperty('age', '13');
// user.sayHi();
//console.log(user)

//  Задание 4.
//
//
//  С помощью цикла for...in вывести в консоль все свойства
//  первого уровня объекта в формате «ключ-значение».

// function add(obj) {
//     for (const userKey in obj ) {
//         if (typeof userKey === 'object' && obj[userKey] !== null)  {
//             add(obj[userKey]);
//         } else {
//             console.log(userKey + '-' +  obj[userKey]);
//         }
//     }
// }
//
// add(user)
//  Продвинутая сложность:
//  Улучшить цикл так, чтобы он умел выводить свойства объекта второго уровня
//  вложенности. */
//
// /**
//
//  TASK 5. IsEmpty. Напишите функцию, которая делает поверхностную проверку объекта на пустоту.
//  /**
//
//  Описание задачи: Напишите функцию, которая делает поверхностную проверку
//  объекта на пустоту.
//  Ожидаемый результат: ({}) => true, ({ a: undefined }) => true, ({ a: 1 }) =>
//  false
//  Пустые значения: '', null, NaN, undefined
//  Сложность задачи: 2 of 5
//
//  @param {Object} object - объект с примитивами
//  @returns {boolean} */
//
// export const isEmpty = (object) => { // Напишите здесь свое решение };


//     TASK 6. Intersection. Напишите функцию, которая поверхностно находит пересечения объектов и возвращает объект пересечений.
//         /**
//
//          Описание задачи: Напишите функцию, которая поверхностно находит пересечения
//          объектов и возвращает объект пересечений.
//          Ожидаемый результат: ({ a: 1, b: 2 }, { c: 1, b: 2 }) => { b: 2 }
//
//          @param {Object<string | number>} firstObj - объект с примитивными значениями
//
//          @param {Object<string | number>} secondObj - объект с примитивными
//          значениями
//          @returns {Object} */
//
//         export const intersection = (firstObject, secondObject) => { // Напишите здесь
//         свое решение
//     };
//     const data = { a: 1, b: 2 }; const data2 = { c: 1, b: 2 };
//     console.log(intersection(data, data2)); // { b: 2 }
//Напишите функцию, которая будет создавать объекты, описывающие товары. У товара
// должны быть такие свойства:
//
// name;
// fullName;
// article;
// price. При этом при попытке напрямую (через точку) изменить свойство price
// происходит его его проверка на прравильность: цена должна быть целым
// положительным числом. Если эти требования нарушаются - присвоения не
// произойдет. Создавать его аналог через "_price нельзя".
//
// Пример работы:
// const notebook = createProduct("lenovo X120S", "lenovo X120S (432-44) W", 3332,
// 23244);
// console.log(notebook.price);// выведет 23244
// notebook.price = -4; // присвоение не произойдет
// console.log(notebook.price);// выведет 23244
// notebook.price = 22000;
// console.log(notebook.price);// выведет 22000
//
// const createItem = (name, fullName, article, price) => {
//     return {
//         name,
//         fullName,
//         article,
//         price,
//     }
//
// };
//
// const notebook = createItem("lenovo X120S", "lenovo X120S (432-44) W", 3332, 23244);
// console.log(notebook.price);
//
// let newPrice;
// Object.defineProperty(notebook, 'price', {
//     get() {
//         return newPrice;
//     },
//     set(value) {
//         if (value > 0 && Number.isInteger(value)) {
//             newPrice = value;
//         }
//
//     },
// });
// notebook.price = 15;
// console.log(notebook.price);
// console.log(notebook)

//TASK 1
// Создать функцию, которая проверяет пустой ли обьект (есть ли хоть какие-то поля
// внутри обьекта ). Функция должна возвращать true если поля есть, иначе false.
//
//
// function checkObj (obj)  {
//     for (let key in obj) {
//         return false;
//     } return true;
// }
//
// console.log(checkObj({}));
//
//
// const isEmpty = (obj) => (Object.keys(obj).length !== 0); // !!Object.keys(obj).length
// console.log(isEmpty({}))
// Напишите функцию, которая принимает аргументом обьект и проверяет сколько в нем
// полей с примитивными значениями, а сколько полей со структурными значениями
// .

// const makeCount = (obj) => {
//     let count = 0;
//     let count2 = 0;
//     for (let i = 0; i < Object.values(obj).length; i++) {
//         if (typeof Object.values(obj)[i] === 'object') {
//             count += 1;
//         } else {count2 += 1;}
//     }
//     console.log(count, count2);
// }
// makeCount({a:1, b: 2, c: {},})
// TASK 3
// Создайте объект city1 (const city1 = {}), укажите у него свойства name (название
// города, строка) со значением «ГородN» и population (населенность города, число)
// со значением 10 млн.


// TASK 4
// Дополните предыдущую задачу. Создайте объект city2 через нотацию {name:
// "ГородM", population: 1e6}.
//
// TASK 5
// Дополните предыдущую задачу. Создайте у объектов city1 и city2 методы getName(),
// которые вернут соответствующие названия городов
// const city1 = {
//    }
// city1.name = 'Kyiv';
// city1.population = 1e6;
//
//
// const city2 = {
//     name: 'Lviv',
//     population: 1e6,
//
// }
// const getName = () => {return this.name};
//
//
// const exportStr = () => { return `name=${this.name} \n population= ${this.population}`}
// city1.exportStr = exportStr;
//
// console.log(city1.exportStr)
// console.log(city1, city2)


// TASK
// Дополните предыдущую задачу. Создайте методы exportStr() у каждого из объектов.
// Этот метод должен возвращать информацию о городе в формате «name=Город n
// npopulation=10000000 \n». Для второго города будет строка со своими значениями.
// Примечание: можно обращаться к каждому свойству через цикл for/in, но методы
// объекта возвращать не нужно
//
// TASK 7
// Создайте базу данных заработных плат сотрудников в виде одного обьекта {name1:
// sallary1, name2: sallary2, name3: sallary3, ...}. Данные запрашиваются у
// пользователя. В качестве названия поля используются имена сотрудников, значение
// поля - заработная плата.
//
// TASK 8
// Создайте глобальную функцию getObj(), которая возвращает this. А у каждого из
// объектов city1 или city2 метод getCity, который ссылается на getObj. Проверьте
// работу метода. Примечание: к объекту вызова можно обратиться через this.
//
// TASK 9
// Создать объект obj, с методами method1(),method2() и method3(). В методе
// method3() должна возвращаться строка «метод3». Сделайте так, чтобы было возможно
// выполнение кода obj.method1().method2().method3().

//Задание 1.
// Написать имплементацию встроенной функции строки repeat(times).
// Функция должна обладать двумя параметрами:
// Целевая строка для поиска символа по индексу;
// Количество повторений целевой строки.
// Функция должна возвращать преобразованную строку.
// Условия:
// Генерировать ошибку, если первый параметр не является строкой, а второй не
// является числом. */

// const repeatTimes = (str, count) => {
//     if (typeof str !== 'string' || typeof count !== 'number') {
//         console.log('Error');
//
//     } else {
//         let newStr = '';
//         for (let i = 0; i < count; i++) {
//             newStr += str;
//         }
//         return newStr;
//     }
// }
//
// console.log(repeatTimes(4, 4));

//Задание 2.
// Написать функцию, capitalizeAndDoublify, которая переводит
// символы строки в верхний регистр и дублирует каждый её символ.
// Условия:
// Использовать встроенную функцию repeat;
// Использовать встроенную функцию toUpperCase;
// Использовать цикл for...of. */
//
// const capitalizeAndDoublify = (str) => {
//     let result = '';
//     for (let strElement of str) {
//
//         result += strElement.toUpperCase().repeat(2);
//     }
//     return result;
// }
// console.log(capitalizeAndDoublify("wow"));
//
// Задание 3.
// Написать функцию truncate, которая «обрезает» слишком длинную строку, добавив
// в конце троеточие «...».
// Функция обладает двумя параметрами:
//     Исходная строка для обрезки;
// Допустимая длина строки.
//     Если строка длиннее, чем её допустимая длина — её необходимо обрезать,
//     и конкатенировать к концу символ троеточия так, чтобы вместе с ним длина
// строки была равно максимально допустимой длине строки из второго параметра.
//     Если строки не длиннее, чем её допустимая длина. */

// const truncate = (str, maxLength) => {
//     if ( str.length > maxLength) {
//         return  str.slice(0, maxLength - 3) + '...';
//     } return str;
// }
// console.log(truncate('hollll'), 2);

// const truncate = (str, maxLength) =>
//     str.length > maxLength ? str.slice(0, maxLength - 3) + '...' : str;
//
// console.log(truncate('hjjjj', 3));
//
// Задание 4.
// Написать функцию-помощник кладовщика.
//     Функция обладает одним параметром:
//     Строка со списком товаров через запятую (water,banana,black,tea,apple).
//     Функция возвращает строку в формате ключ-значение, где ключ — имя товара, а
// значение — его остаток на складе.
//     Каждый новый товар внутри строки должен содержатся на новой строке.
//     Если какого-то товара на складе нет, в качестве остатка указать «not found».
// Условия:
//     Имя товара не должны быть чувствительно к регистру;
// Дополнительных проверок совершать не нужно. */
// const wareHouse = (str) => {
//    let arr = str.split(',');
//    let  result = '';
//    for (let i = 0; i < arr.length; i++) {
//        let number = Math.round(Math.random()*10);
//        result =+ `${arr[i]} - ${number === 0 ? ' not found' : number } \n ` ;
//    } return result;
// }
//
// console.log(wareHouse('water,banana,black,tea,apple'))

// let a = prompt('birthday');
// let arr = a.split('.');
// console.log(arr)

// let d = '20.12.2022'
// const formatDate = (d) => {
//     let arr = d.split('.')
//     return new Date(`${arr[1]}.${arr[0]}.${arr[2]}`);
// }
// console.log(formatDate(d))

//Задача 1.
// Дан массив с днями недели.
// Вывести в консоль все дни недели с помощью:
// Классического цикла for;
// Цикла for...of;
// Специализированного метода для перебора элементов массива forEach. */
// const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday',
//     'Friday', 'Saturday', 'Sunday',];

// days.forEach(value => console.log(value));
// for (let i = 0; i < days.length; i++) {
//     console.log(days[i]);
// }
// for (const day of days) {
//     console.log(day)
// }


//
// const getLang = () => {
//     let answerUser;
//     do {
//         answerUser = prompt('Enter yor lang')
//     }
//     while (answerUser !== 'ua' && answerUser !== 'ru' && answerUser !== 'en');
//     return answerUser;
// }
//
//
// const days = {
//     ua: ['Понеділок', 'Вівторок', 'Середа', 'Четвер',
//         'П’ятниця', 'Субота', 'Неділя',],
//     ru: ['Понедельник', 'Вторник', 'Среда',
//         'Четверг', 'Пятница', 'Суббота', 'Воскресенье',],
//     en: ['Monday', 'Tuesday',
//         'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday',],
// }

//     Object.keys(object).filter(getLang)
//     return Object.value(arrayN);
//
//
//
// }
// console.log(multiLang(days));

// const arr = [
//     {
//         name: 'John',
//         age: 31,
//         city: 'NY',
//     },
//     {
//         name: 'Sam',
//         age: 24,
//         city: 'LA',
//     },
//     {
//         name: 'Bob',
//         age: 45,
//         city: 'NY',
//     },
//     {
//         name: 'Edward',
//         age: 31,
//         city: 'LA',
//     },
// ];
//
// const filteredByAge = arr.filter((elem) => elem.age === 31);
// const filteredByCity = arr.filter((elem) => elem.city === 'LA');
//
// console.log('filteredByAge: ', filteredByAge);
// console.log('filteredByCity: ', filteredByCity);
// console.log('arr: ', arr);


//Напишите функцию mergeArrays для объединения нескольких массивов в один.
// Функция обладает неограниченным количеством параметров. Функция возвращает один
// массив, который является сборным из массивов, переданных функции в качестве
// аргументов при её вызове.
// Условия:
//
// Все аргументы функции должны обладать типом «массив», иначе генерировать
// ошибку;
// В ошибке обязательно указать какой по счёту аргумент провоцирует ошибку.
// Заметки:
// Делать поддержку выравнивания вложенных массивов (флеттенинг) не нужно
//
// const mergeArrays = (...arg) => {
//     let i = [];
//    arg.map((value, index) => {
//         if (!Array.isArray(value)) {
//             console.log('error' + index);
//         }
//
//     })
//
//
//     // for (const argElement of arg) {
//     //     if (!Array.isArray(argElement)) {
//     //         console.log('error')
//     //         break;
//     //     }
//     // }         return  [].concat(...arg)
//
//
// }
// console.log(mergeArrays([1, 2], [3], 5));

// function mergeArrays(...arr) {
//     const noArrIndexes = arr.map((el, index) => (!Array.isArray(el) ? index : null)).filter((el) => el !== null);
//
//     if (noArrIndexes.length) {
//         return `Error in indexes ${noArrIndexes.toString()}`;
//     }
//
//     return [].concat(...arr);
// }
// console.log(mergeArrays([1, 3, 4], [1, 6, 8], [23, 34, 45]));
//
// document.body.style.background = 'red';
