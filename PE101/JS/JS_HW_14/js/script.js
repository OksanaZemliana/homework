'use strict';
const buttonChange = document.querySelector('.btn-header');
buttonChange.addEventListener('click', (event)=>{
    event.preventDefault();
    document.body.classList.toggle('orange');
    const theme = document.body.classList.contains("orange") ? "orange" : "white";
        localStorage.setItem("class", theme);
})
document.addEventListener("DOMContentLoaded",  ()=> {
    if (localStorage.getItem("class")) {
        const theme = localStorage.getItem("class") === "orange" ? "orange" : "white";
        document.body.classList.toggle(theme);
    }
});