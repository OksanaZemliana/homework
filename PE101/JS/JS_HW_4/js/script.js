'use strict';
// Теоретичні питання
// Описати своїми словами навіщо потрібні функції у програмуванні.
// Функції використовуються для того , щоб не повторювати один і той же код в програмі або викликати його за потреби.

//     Описати своїми словами, навіщо у функцію передавати аргумент.
//Аргументы в функцію передаються  для того, щоб присвоїти їх значчення параметрам функції, в іншому випадку значення буде не визначене.

//     Що таке оператор return та як він працює всередині функції?
// Цей оператор завершає виконання функції та повертає значення її виконання.

//     Завдання
//     Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами. Завдання має бути
//     виконане на чистому Javascript без використання бібліотек типу jQuery або React.
//
//     Технічні вимоги:
//     Отримати за допомогою модального вікна браузера два числа.
//     Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
// Створити функцію, в яку передати два значення та операцію.
//     Вивести у консоль результат виконання функції.
//     Необов'язкове завдання підвищеної складності
// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів числа, або при вводі вказав не числа,
//- запитати обидва числа знову (при цьому значенням за замовчуванням для кожної зі змінних має бути введена інформація раніше).

const getUserNumber = (message) => {
    let userNumber;
    do {
        userNumber = prompt(message, userNumber);
    } while (isNaN(+userNumber) || !userNumber);
    return +userNumber;
}

const getUserSign = () => {
    let userSign;
    do {
        userSign = prompt("Enter your sign. One of: + , - , * , / ", userSign);
    } while (userSign !== '+' && userSign !== '-' && userSign !== '*' && userSign !== '/');
    return userSign;
}

const firstUserNumber = () => getUserNumber('Enter your first number');
const secondUserNumber = () => getUserNumber('Enter your second number');

const calcMathOperation = (firstNumber, secondNumber, sign) => {
    switch (sign) {
        case '+':
            return firstNumber + secondNumber;
        case '-':
            return firstNumber - secondNumber;
        case '*':
            return firstNumber * secondNumber;
        case '/':
            return firstNumber / secondNumber;
    }
}

console.log(calcMathOperation(firstUserNumber(), secondUserNumber(), getUserSign()));

