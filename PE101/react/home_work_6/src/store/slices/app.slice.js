import { createSlice } from "@reduxjs/toolkit";

const appSlice = createSlice({
	name: 'app',
	initialState: {
		isOpenModal: false,
		currentData: {},
		isOpenForm: false,
		isOpenButton: true,
	},
	reducers: {
		actionModal: (state, action) => {
			state.isOpenModal = action.payload;
		},

		actionSetCurrentData: (state, action) => {
			state.currentData = {...action.payload};
		},
		actionForm : (state, action)=>{
			state.isOpenForm = action.payload;
		},
		actionButton : (state, action)=>{
			state.isOpenButton = action.payload;
		},
	}
});

export const { actionModal, actionSetCurrentData, actionForm, actionButton } = appSlice.actions;

export default appSlice.reducer;