import reducer, {actionAddToFavorite, actionSetFavorite} from './favorite.slice'

describe('favorite reducer test', ()=> {
	test('should initial state ', ()=> {
		expect(reducer(undefined, {type: undefined})).toEqual({data: []})
	})

	test('should update state ', ()=> {
		const previousState = {}
		expect(reducer(previousState, actionSetFavorite([{id:1, name:"test"}]))).toEqual({data: [{id:1, name:"test"}]})
	})

	test('should add favorite state ', ()=> {
		expect(reducer({data:[]}, actionAddToFavorite({id:1, name:"test"}))).toEqual({data: [{id:1, name:"test"}]})
	})

	test('should remove favorite state if it is', ()=> {
		expect(reducer({data:[{id:1, name:"test"}]}, actionAddToFavorite({id:1, name:"test"}))).toEqual({data: []})
	})
})