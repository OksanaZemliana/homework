import reducer, {actionAddCards} from './card.slice'


describe('card reducer test', ()=> {
	test('should initial state ', ()=> {
		expect(reducer(undefined, {type: undefined})).toEqual({data: []})
	})

	test('should update state ', ()=> {
			expect(reducer({data: []}, actionAddCards([{id:1, name:"test"}]))).toEqual({data: [{id:1, name:"test"}]})
	})

})