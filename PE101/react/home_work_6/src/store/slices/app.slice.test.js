import reducer, {actionModal, actionSetCurrentData, actionForm, actionButton} from './app.slice'

const initialState = {
	isOpenModal: false,
	currentData: {},
	isOpenForm: false,
	isOpenButton: true,
};

describe('app slice test', () => {
	test('should initial state ', () => {
		expect(reducer(undefined, {type: undefined})).toEqual(initialState)
	})

	test('should update isModal state', ()=>{
		expect(reducer(initialState, actionModal(true))).toEqual({isOpenModal: true,
			currentData: {},
			isOpenForm: false,
			isOpenButton: true,})
	})

	test('should update isOpenForm state', ()=>{
		expect(reducer(initialState, actionForm(true))).toEqual({isOpenModal: false,
			currentData: {},
			isOpenForm: true,
			isOpenButton: true,})
	})

	test('should update isOpenButton state', ()=>{
		expect(reducer(initialState, actionButton(false))).toEqual({isOpenModal: false,
			currentData: {},
			isOpenForm: false,
			isOpenButton: false,})
	})

	test('should update currentData state', ()=>{
		expect(reducer(initialState, actionSetCurrentData({id:1, name: 'test'}))).toEqual({isOpenModal: false,
			currentData: {id:1, name: 'test'},
			isOpenForm: false,
			isOpenButton: true,})
	})
})