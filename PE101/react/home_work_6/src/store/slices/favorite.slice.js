import {createSlice} from "@reduxjs/toolkit";

const favoriteSlice = createSlice({
	name: 'favorite',
	initialState: {
		data: [],
	},

	reducers: {
		actionSetFavorite: (state, action)=>{
			state.data = action.payload;
		},

		actionAddToFavorite: ( state, action) => {
		 	const index = state.data.findIndex((el) => el.id ===  action.payload.id)

				if (index === -1) {
					state.data.push(action.payload)
				}
				else {
					state.data.splice(index, 1)
				}
				localStorage.setItem(`favoriteItems`, JSON.stringify(state.data));
			}
	}
})


export const {actionAddToFavorite, actionSetFavorite} = favoriteSlice.actions

export default favoriteSlice.reducer