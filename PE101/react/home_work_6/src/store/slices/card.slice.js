import {createSlice} from "@reduxjs/toolkit";
import {actionSetFavorite} from './favorite.slice'
import {actionSetItems} from './cart.slice'


const cardSlice = createSlice({

	name: 'cards',
	initialState: {
		data: [],
	},

	reducers: {
		actionAddCards: (state, action) => {
			state.data = action.payload;
		},

	}
})

export const {actionAddCards} = cardSlice.actions

export const actionFetchCards = () => async (dispatch) => {

	const items = await fetch('./data.json').then(res => res.json());
	dispatch(actionAddCards(items));

	const favoriteItems = JSON.parse(localStorage.getItem('favoriteItems'))?.map(({id}) => id);
	if (favoriteItems) {
		const itemsToLS = items.filter((item) => favoriteItems.includes(item.id));
		dispatch(actionSetFavorite(itemsToLS))
	}
	if (localStorage.getItem('carts')) {
		const carts = JSON.parse(localStorage.getItem(`carts`))?.map((el) => {
			const index = items.findIndex((item) => item.id === el.id);
			if (index !== -1) {
				return {...items[index], count: el.count}
			}
		})
		dispatch(actionSetItems(carts))
	}
}

export default cardSlice.reducer

