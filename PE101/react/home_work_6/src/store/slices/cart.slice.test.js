import reducer, {
	actionAddToCart,
	actionIncrementCartItem,
	actionDecrementCartItem,
	actionDeleteCartItem,
	actionSetItems
} from './cart.slice'


const initialState = {
	items: [],
}


const itemOne = {id: '1', name: "test"};
const itemTwo = {id: '2', name: "test2"}
const prevState = {items: [{...itemOne, count: 1}]}

describe('app slice test', () => {
	test('should initial state ', () => {
		expect(reducer(undefined, {type: undefined})).toEqual(initialState)
	})

	test('should update  state', () => {
		expect(reducer(initialState, actionSetItems([itemOne]))).toEqual({
			items: [itemOne],
		})
	})

	test('should add to cart new item ', () => {
		expect(reducer(initialState, actionAddToCart(itemOne))).toEqual({
			items: [{...itemOne, count: 1}],
		})
	})

	test('should add to cart item has it', () => {
		expect(reducer(prevState, actionAddToCart(itemOne))).toEqual({
			items: [{...itemOne, count: 2}],
		})
	})

	test('should add to cart new another item ', () => {
		expect(reducer(prevState, actionAddToCart(itemTwo))).toEqual({
			items: [{...itemOne, count: 1}, {...itemTwo, count: 1}],
		})
	})

	test('should increase item to cart ', () => {
		expect(reducer(prevState, actionIncrementCartItem(itemOne.id))).toEqual({
			items: [{...itemOne, count: 2}]
		})
	})

	test('should decrease item to cart ', () => {
		const prev = {items: [{...itemOne, count: 2}]}
		expect(reducer(prev, actionDecrementCartItem(itemOne.id))).toEqual({
			items: [{...itemOne, count: 1}]
		})
	})

	test('should delete item from cart ', () => {
		expect(reducer(prevState, actionDeleteCartItem(itemOne))).toEqual({
			items: []
		})
	})
})