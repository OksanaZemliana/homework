import './FavoritePage.scss'
import Card from "../../components/WrapperCard/Card/Card";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/Button";
import {useDispatch, useSelector} from "react-redux";
import {actionModal} from "../../store/slices/app.slice";
import {actionAddToCart} from "../../store/slices/cart.slice";

const FavoritePage = () => {

	const dispatch = useDispatch();
	const cardsFavorite = useSelector(store => store.favorite.data);
	const isOpenModal = useSelector(store => store.app.isOpenModal);
	const currentCard = useSelector(store => store.app.currentData);
	const closeModal = () => dispatch(actionModal(false));


	return (
		<>
			<h1>Улюблені книги</h1>
			<ul className='list-favorite'>
				{cardsFavorite?.map(({url, name, author, price, id}) => (
					<li key={id}>
						<Card
							isFavorite={cardsFavorite?.some((el) => el.id === id)}
							id={id}
							url={url}
							name={name}
							author={author}
							price={price}
							className='card'
						/>
					</li>
				))}
			</ul>

			{isOpenModal && (
				<Modal isCloseButton
					   headerModal={'Ви дійсно хочете додати книгу в кошик:'}
					   action={
						   <div className='wrapper-btn'>
							   <Button type={"button"} className='btn-modal'
									   handleClick={() => {
										   {
											   dispatch(actionAddToCart(currentCard))
											   closeModal();
										   }
									   }}
							   >В кошик</Button>
							   <Button type={"button"} className='btn-modal'
									   handleClick={closeModal}>Відмінити</Button>
						   </div>
					   }
				/>)
			}
		</>
	)
}

export default FavoritePage;