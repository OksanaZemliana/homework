import './CartPage.scss'
import Button from "../../components/Button/Button";
import Modal from "../../components/Modal/Modal";
import CardFromCart from "../../components/CardFromCart/CardFromCart";
import {useDispatch, useSelector} from "react-redux";
import {actionModal, actionForm, actionButton} from "../../store/slices/app.slice";
import {actionDeleteCartItem} from "../../store/slices/cart.slice";
import FormCart from "../../components/Form/FormCart";
import {useState} from "react";

const CartPage = () => {
	const dispatch = useDispatch();
	const [isSubmitOrder, setIsOpenSubmitOrder] = useState(false);
	const carts = useSelector(store => store.carts.items);
	const isOpenModal = useSelector(store => store.app.isOpenModal);
	const isOpenForm = useSelector(store => store.app.isOpenForm);
	const isOpenButton = useSelector(store => store.app.isOpenButton);
	const currentCard = useSelector(store => store.app.currentData);

	const list = carts?.map(({url, name, count, price, id}) => (
		<li key={id}>
			<CardFromCart
				url={url}
				name={name}
				count={count}
				id={id}
				price={price}
			/>
		</li>
	))

	const totalAmount = carts?.map(({price, count}) => price * count).reduce((prev, curr) => prev + curr, 0);

	const handleTitleSubmit = () => {
		setIsOpenSubmitOrder(true);

	}

	return (
		<>
			{!isSubmitOrder ? <h1>Ваш кошик</h1> : <h1>Ваше замовлення оформлено</h1>}
			<ul className='list-cart'>
				{list}

				{!isSubmitOrder ?
					<span className='total-price'>Всього: {totalAmount} грн</span>
					: null}
			</ul>
			{isOpenButton ?
				<div className='wrapper-buy'>
					<Button className='button-buy' handleClick={() => {
						dispatch(actionForm(true));
						dispatch(actionButton(false))
					}}>Купити</Button></div> : null}


			{isOpenForm && <FormCart totalAmount={totalAmount} handleTitleSubmit={handleTitleSubmit}/>}

			{isOpenModal && (
				<Modal isCloseButton
					   headerModal={'Ви дійсно хочете видалити книгу з кошика:'}
					   action={
						   <div className='wrapper-btn'>
							   <Button type={"button"} className='btn-modal'
									   handleClick={() => {
										   dispatch(actionDeleteCartItem(currentCard));
										   dispatch(actionModal(false));
									   }}
							   >Видалити</Button>
							   <Button type={"button"} className='btn-modal'
									   handleClick={() => {
										   dispatch(actionModal(false))
									   }}>Відмінити</Button>
						   </div>
					   }
				/>)
			}
		</>
	)
}

export default CartPage;