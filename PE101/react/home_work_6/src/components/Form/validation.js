import * as yup from "yup"

export const validationSchema = yup.object({
	name: yup
		.string()
		.min(2, "Занадто коротке ім'я")
		.matches(/[a-zA-ZА-Яа-я]/, "Поле містить тільки літери")
		.required("Поле обов'язкове для заповнення"),
	lastName: yup
		.string()
		.min(2, "Занадто коротке ім'я")
		.matches(/[a-zA-ZА-Яа-я]/, "Поле містить тільки літери")
		.required("Поле обов'язкове для заповнення"),
	age: yup
		.number()
		.min(15, "Ваш вік не менше 15 років")
		.max(150, "Не вірний вік")
		.required("Поле обов'язкове для заповнення"),
	address: yup
		.string()
		.min(10, "Занадто коротка адреса")
		.required("Поле обов'язкове для заповнення"),
	phone: yup
		 .string()
		.matches( /^\d{1,10}$/, "Поле містить тільки цифри ")
		.min(10, "Введено не повний номер")
		.max(10, "Занадто довгий номер")
		.required("Поле обов'язкове для заповнення"),

})