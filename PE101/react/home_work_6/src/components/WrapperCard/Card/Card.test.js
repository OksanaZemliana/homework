import Card from './Card'
import {render, screen} from '@testing-library/react'
import {Provider} from "react-redux";
import store from "../../../store";
import '@testing-library/jest-dom'


describe('Card component', () => {
	test('should snapshot', () => {
		const {asFragment} = render(
			<Provider store={store}>
				<Card id='111' url='url' name='Item' author='Author' price='55'/>
			</Provider>);

		expect(asFragment()).toMatchSnapshot();
	})

	test('should icon with isFavorite', () => {
		const {container} = render(<Provider store={store}>
				<Card isFavorite/>
			</Provider>);
		expect(container.querySelector('svg')).toHaveClass('icon-starFill');
	})
})