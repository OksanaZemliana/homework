import Header from "./Header"
import {render} from '@testing-library/react'
import {Provider} from "react-redux";
import store from "../../store";
import {BrowserRouter} from 'react-router-dom'


describe('Header component', () => {
	test("should snapshot Header", () => {
		const {asFragment} = render(
			<Provider store={store}>
				<BrowserRouter>
					<Header/>
				</BrowserRouter>
			</Provider>)

		expect(asFragment()).toMatchSnapshot()
	})
})