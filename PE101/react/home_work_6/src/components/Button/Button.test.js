import Button from './Button'
import {fireEvent, render, screen} from "@testing-library/react"
import '@testing-library/jest-dom'


const handleClick = jest.fn();

describe('Button test work', () => {
	test('should Button snapshot', () => {
		const {asFragment} = render(<Button type={'submit'} className='btn-test'>SUBMIT</Button>)
		expect(asFragment()).toMatchSnapshot()
	})

	test('should handle click works', () => {
		render(<Button type={'submit'} className='btn-test' handleClick={handleClick}>SUBMIT</Button>)
		const btn = screen.getByText("SUBMIT");
		fireEvent.click(btn)
		expect(handleClick).toHaveBeenCalled()
	})

	test('should disabled', () => {
		render(<Button disabled={true}>disabled</Button>)
		const btn = screen.getByText("disabled");
		expect(btn).toBeDisabled()
	})

	test('should have the className', () => {
		render(<Button className={'test'}>TEST</Button>)
		const btn = screen.getByText("TEST");

		expect(btn).toHaveClass('test')
	})

	test('should have children', () => {
		render(<Button>TEST</Button>)
		const btn = screen.getByText("TEST");
		expect(btn).not.toBeEmptyDOMElement()
	})
})