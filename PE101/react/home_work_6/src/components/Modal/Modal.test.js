import Modal from './Modal'
import {fireEvent, render, screen} from '@testing-library/react'
import {Provider,useDispatch } from "react-redux";
import store from "../../store";
import {actionModal} from "../../store/slices/app.slice";
import '@testing-library/jest-dom'


const ComponentTest = ({headerModal, action, isCloseButton}) => {
	const dispatch = useDispatch();
	return (
		<>
			<button onClick={ () => dispatch(actionModal(true))} >Open</button>
			<button onClick={ () => dispatch(actionModal(false))} >Close</button>
			<Modal headerModal={headerModal} action={action}isCloseButton={isCloseButton} />
		</>
	)
}

const MockedProvider = ({headerModal, action, isCloseButton}) => (
	<Provider store={store}>
		<ComponentTest headerModal={headerModal} action={action} isCloseButton={isCloseButton} />
	</Provider>
)

describe('Modal test component', () => {
	test('should Modal snapshot', () => {
			const {asFragment} = render(<MockedProvider headerModal={'Add to cart'} action={<><p>TEST</p></>}/>)
			expect(asFragment()).toMatchSnapshot()
		}
	)

	test('should open Modal', () => {
		render(<MockedProvider action={<><p>TEST</p></>}/> )

		fireEvent.click(screen.getByText('Open'));
		expect(screen.getByText('TEST')).toBeInTheDocument()
	})

	test('should close Modal', () => {
		render(<MockedProvider headerModal={'Add to cart'}  action={<><p>TEST</p></>}/> )

		fireEvent.click(screen.getByText('Close'));
		expect(screen.queryByText(`<p>TEST</p>`)).not.toBeInTheDocument()
	})

	test('should render Modal with closeIcon and close by closeIcon', () => {
		const {container} = render(<MockedProvider isCloseButton={true} action={<><p>TEST</p></>}/> )

		expect(container.querySelector('.iconModal')).toBeInTheDocument();

		fireEvent.click(container.querySelector('.iconModal'));
		expect(screen.queryByText(`<p>TEST</p>`)).not.toBeInTheDocument()

	})

	test('should close Modal by click bg', () => {
		const {container} = render(<MockedProvider action={<><p>TEST</p></>}/> )

		fireEvent.click(container.querySelector('.wrapperModal'));
		expect(screen.queryByText(`<p>TEST</p>`)).not.toBeInTheDocument()

	})
})
