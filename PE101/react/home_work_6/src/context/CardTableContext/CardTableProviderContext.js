import { useState } from 'react';
import CardTableContext from './CardTableContext';

const CardTableContextProvider = ({children}) => {

	const [isCardView, setIsCardView] = useState(true);

	return (
		<CardTableContext.Provider value={{ isCardView, setIsCardView }}>
			{children}
		</CardTableContext.Provider>
	);
}

export default CardTableContextProvider;