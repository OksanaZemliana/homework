import { createContext } from "react";

const CardTableContext = createContext({});

export default CardTableContext;