import {actionSetItems} from "../store/slices/cart.slice";
import {actionSetFavorite} from "../store/slices/favorite.slice";


const setFromLS= () => (dispatch)=>{

	const carts = localStorage.getItem('carts')
	if (carts) {
		dispatch(actionSetItems(JSON.parse(carts)))

	}
	const cardsFavorite = localStorage.getItem('favoriteItems')
	if (cardsFavorite) {
		dispatch(actionSetFavorite(JSON.parse(cardsFavorite)))
	}
}

export default setFromLS;