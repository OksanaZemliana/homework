import {Route, Routes} from "react-router-dom";
import WrapperCard from "./components/WrapperCard/WrapperCard";
import CartPage from "./pages/CartPage/CartPage";
import FavoritePage from "./pages/FavoritePage/FavoritePage";
import NotFoundPage from "./pages/NotFoundPage/NotFoundPage";
import CardTableContextProvider from "./context/CardTableContext/CardTableProviderContext";


const AppRoutes = () => {
	return (
		<Routes>
			<Route index element={
				<CardTableContextProvider>
				<WrapperCard/>
				</CardTableContextProvider>
			}/>
			<Route path={"/cart"} element={<CartPage/>}/>
			<Route path={"/favorite"} element={<FavoritePage/>}/>
			<Route path={"*"} element={<NotFoundPage/>}/>
		</Routes>
	)
}
export default AppRoutes;