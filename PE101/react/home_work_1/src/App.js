import './App.scss';
import Button from "./components/Button/Button";
import {Component} from "react";
import Modal from "./components/Modal/Modal";

class App extends Component {

	state = {
		isOpenFirstModal: false,
		isOpenSecondModal: false,
	}
	handleFirstModal = () => {
		this.setState((prev) => ({...prev, isOpenFirstModal: !prev.isOpenFirstModal}))
	}

	handleSecondModal = () => {
		this.setState((prev) => ({...prev, isOpenSecondModal: !prev.isOpenSecondModal}))
	}


	render() {
		const {isOpenFirstModal, isOpenSecondModal} = this.state;

		return (
			<>
				<Button handleClick={this.handleFirstModal} className={'btn'} buttonTxt={'Open first modal'}
						backgroundColor='orange'/>
				<Button handleClick={this.handleSecondModal} className={'btn'} buttonTxt={'Open second modal'}
						backgroundColor='yellow'/>

				{isOpenFirstModal &&
					<Modal closeModal={this.handleFirstModal} headerModal={'Do you want to delete this file?'}
						   textModal={'Once you delete this file, it won’t be possible to undo this action? Are you sure you want to delete it?'}
						   action={
							   <div className='wrapper-btn'>
								   <Button className={'btn-modal'} buttonTxt={'Ok'}/>
								   <Button className={'btn-modal'} buttonTxt={'Cancel'}
										   handleClick={this.handleFirstModal}/>
							   </div>
						   }
						   isCloseButton
					/>

				}

				{isOpenSecondModal &&
					<Modal closeModal={this.handleSecondModal}
						   headerModal={'Do you want create element?'}
						   textModal={'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A deserunt facere nesciunt repudiandae sint velit.'}
						   action={
							   <div className='wrapper-btn'>

								   <Button className={'btn-modal'} buttonTxt={'Create'}/>
								   <Button className={'btn-modal'} buttonTxt={'Close'}
										   handleClick={this.handleSecondModal}/>
							   </div>
						   }
					/>}

			</>

		);
	}

}

export default App;
