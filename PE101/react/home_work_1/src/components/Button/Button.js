import {PureComponent} from "react";
import  "../../App";
import './Button.scss'

class Button extends PureComponent {


	render (){
		const  {className, handleClick,buttonTxt, backgroundColor} = this.props;

		return (
			<button className={className} onClick={handleClick} style={{backgroundColor}}>{buttonTxt}</button>
		)
	}
}

export default Button;