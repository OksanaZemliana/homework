import Button from "../Button/Button";
import {ReactComponent as DelIcon} from "./icons/delete.svg";
import './CardFromCart.scss'
import PropTypes from "prop-types";
import {actionModal, actionSetCurrentData} from "../../store/slices/app.slice";
import {useDispatch} from "react-redux";
import {actionDecrementCartItem, actionIncrementCartItem} from "../../store/slices/cart.slice";
import Card from "../WrapperCard/Card/Card";


const CardFromCart = ({url, name, count, id, price}) => {
     const dispatch = useDispatch();

	return (
		<>
			<div className='wrapper-img'>
				<img src={url} alt={name}/>
				<p>{name}</p>
			</div>
			<div className='wrapper-btn-cart'>
				<span>{count}</span>
				<Button className='btn-count' handleClick={() => {
					dispatch(actionIncrementCartItem(id))
				}}>+</Button>
				<Button className='btn-count' handleClick={() => {
					dispatch(actionDecrementCartItem(id))
				}}>-</Button>
				<Button handleClick={() => {
					dispatch(actionModal(true))
					dispatch(actionSetCurrentData({id, name, url, price}))
				}}><DelIcon/></Button>
			</div>
			<span>{price} грн</span>
			<div className='total-item'>{price * count} грн</div>

		</>
	)

}
Card.propTypes = {
	id: PropTypes.string,
	url: PropTypes.string,
	name: PropTypes.string,
	count: PropTypes.number,
	price: PropTypes.string,

}

Card.defaultProps = {
	id: '',
	url: '',
	name: '',
	count: 0,
	price: '',

}


export default CardFromCart;