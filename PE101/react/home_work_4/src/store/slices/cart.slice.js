import {createSlice} from "@reduxjs/toolkit";

const cartSlice = createSlice({
	name: "carts",
	initialState: {
		items: [],
	},
	reducers : {
        actionSetItems: (state, {payload}) => {
			state.items = payload;
		},
		actionAddToCart:(state, {payload}) =>  {
			const index = state.items.findIndex(el => el.id === payload.id)
			if (index === -1) {
				state.items.push({...payload, count: 1})
			} else {
				state.items[index].count += 1
			}
			localStorage.setItem("carts", JSON.stringify(state.items))
		},

		actionIncrementCartItem: (state, {payload}) => {
			const index = state.items.findIndex(el => el.id === payload)
			if (index !== -1) {
				state.items[index].count += 1
			}
			localStorage.setItem("carts", JSON.stringify(state.items))
		},

		actionDecrementCartItem : (state, {payload}) => {
			const index = state.items.findIndex(el => el.id === payload)
			if (index !== -1 && state.items[index].count > 1) {
				state.items[index].count -= 1
			}

			localStorage.setItem("carts", JSON.stringify(state.items))
		},
		actionDeleteCartItem : (state, {payload}) => {
			const index = state.items.findIndex(el => el.id === payload.id)

			if (index !== -1) {
				state.items.splice(index, 1);
			}
			localStorage.setItem("carts", JSON.stringify(state.items))
		}
	}
})

export const {actionAddToCart, actionIncrementCartItem, actionDecrementCartItem, actionDeleteCartItem, actionSetItems} = cartSlice.actions

export default cartSlice.reducer