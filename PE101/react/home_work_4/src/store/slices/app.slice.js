import { createSlice } from "@reduxjs/toolkit";

const appSlice = createSlice({
	name: 'app',
	initialState: {
		isOpenModal: false,
		currentData: {}
	},
	reducers: {
		actionModal: (state, {payload}) => {
			state.isOpenModal = payload;
		},

		actionSetCurrentData: (state, {payload}) => {
			state.currentData = {...payload};
		},
	}
});

export const { actionModal, actionSetCurrentData } = appSlice.actions;

export default appSlice.reducer;