import './styles/App.scss'
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import {useEffect, useState} from "react";
import {Route, Routes} from "react-router-dom";
import WrapperCard from "./components/WrapperCard/WrapperCard";
import CartPage from "./pages/CartPage/CartPage";
import FavoritePage from "./pages/FavoritePage/FavoritePage";
import NotFoundPage from "./pages/NotFoundPage/NotFoundPage";

const App = () => {
	const [isOpenModal, setIsOpenModal] = useState(false);
	const [cards, setCards] = useState([]);
	const [carts, setCarts] = useState([]);
	const [currentCard, setCurrentCard] = useState({});
	const [cardsFavorite, setCardsFavorite] = useState([]);

	const handleModal = () => {
		setIsOpenModal(!isOpenModal);
	}

	useEffect(() => {
		(async () => {
			const items = await fetch('./data.json').then(res => res.json());
			setCards(items);

			const carts = localStorage.getItem('carts')
			if (carts) {
				setCarts(JSON.parse(carts))
			}
			const cardsFavorite = localStorage.getItem('favoriteItems')
			if (cardsFavorite) {
				setCardsFavorite(JSON.parse(cardsFavorite))
			}
		})();
	}, [])

	const setCurrentItem = (value) => {
		setCurrentCard(value)
	}

	const addToCart = (card) => {
		setCarts((current) => {
			const carts = [...current]

			const index = carts.findIndex(el => el.id === card.id)

			if (index === -1) {
				carts.push({...card, count: 1})
			} else {
				carts[index].count += 1
			}

			localStorage.setItem("carts", JSON.stringify(carts))
			return carts
		})
	}

	const incrementCartItem = (id) => {
		setCarts((current) => {
		    const carts = [...current]

		    const index = carts.findIndex(el => el.id === id)

		    if (index !== -1) {
		        carts[index].count += 1
		    }

		    localStorage.setItem("carts", JSON.stringify(carts))
		    return carts
		})
	}

	const decrementCartItem = (id) => {
		setCarts((current) => {
			const carts = [...current]

			const index = carts.findIndex(el => el.id === id)

			if (index !== -1 && carts[index].count > 1) {
				carts[index].count -= 1
			}

			localStorage.setItem("carts", JSON.stringify(carts))
			return carts
		})
	}

	const addToFavorite = (item) => {
		setCardsFavorite((current) => {
			let carts = [...current]
			const itemFind = carts.find((el) => el.id === item.id)
			if (!itemFind) {
				carts.push(item)
			} else {
				carts = carts.filter((el) => el.id !== item.id)
			}

			localStorage.setItem(`favoriteItems`, JSON.stringify(carts));
			return carts
		})
	}

	const deleteCartItem = (item) => {
		setCarts((current) => {
			const carts = [...current]

			const index = carts.findIndex(el => el.id === item.id)

			if (index !== -1) {
				carts.splice(index, 1);
			}

			localStorage.setItem("carts", JSON.stringify(carts))
			return carts
		})
	}

	return (
		<>
			<Header counterCart={carts.map(({count}) => count).reduce((prev, curr) => prev + curr, 0)}
					counterFavorite={cardsFavorite.length}/>
			<section>
				<Routes>
					<Route index element={<WrapperCard
						cards={cards}
						cardsFavorite = {cardsFavorite}
						addToFavorite = {addToFavorite}
						setCurrentCard = {setCurrentItem}
						handleModal={handleModal}
						isOpenModal = {isOpenModal}
						currentCard = {currentCard}
						addToCart = {addToCart}
					/>}/>
					<Route path={"/cart"} element={<CartPage
						incrementCartItem={incrementCartItem}
						decrementCartItem={decrementCartItem}
						deleteCartItem={deleteCartItem}
						carts={carts}
						handleModal={handleModal}
						currentCard={currentCard}
						setCurrentCard={setCurrentItem}
						isOpenModal={isOpenModal}
					/>}/>
					<Route path={"/favorite"} element={<FavoritePage
						handleModal={handleModal}
						isOpenModal = {isOpenModal}
						addToFavorite = {addToFavorite}
						setCurrentCard = {setCurrentCard}
						cardsFavorite = {cardsFavorite}
						currentCard={currentCard}
						addToCart={addToCart}
					/>}/>
					<Route path={"*"} element={<NotFoundPage/>}/>
				</Routes>
			</section>
			<Footer/>
		</>
	);
}

export default App;
