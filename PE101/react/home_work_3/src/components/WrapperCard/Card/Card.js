import Button from "../../Button/Button";
import {ReactComponent as StarIcon} from './icons/star.svg'
import {ReactComponent as StarIconFill} from './icons/star_fill.svg'
import PropTypes from "prop-types";
import './Card.scss'


const Card =(props)=> {

		const {
			id,
			url,
			name,
			author,
			price,
			handleModal,
			setCurrentCard,
			addToFavorite,
			isFavorite,
		} = props

		return (
			<div className='card'>
				<img src={url} alt={name}/>
				<Button className={'btn-star'} handleClick={() => {
					addToFavorite({id, name, url,price, author})}}>
					{!isFavorite ? <StarIcon/> :<StarIconFill className='icon-starFill'/>}
				</Button>


				<h2>{name}</h2>
				<p>{author}</p>
				<span>{price} грн</span>
				<Button type={'button'} handleClick={() => {
					handleModal();
					setCurrentCard({id, name, url,price, author})
				}}
						className={'btn-card'}>До кошика</Button>
			</div>
		)
}

Card.propTypes = {
	id:PropTypes.string,
	url: PropTypes.string,
	name: PropTypes.string,
	author: PropTypes.string,
	price: PropTypes.string,
	handleModal: PropTypes.func,
	setCurrentCard: PropTypes.func,
	addToFavorite: PropTypes.func,
	isFavorite: PropTypes.bool,
}

Card.defaultProps = {
	id: '',
	url: '',
	name: '',
	author: '',
	price: '',
	handleModal: () => {
	},
	setCurrentCard: () => {
	},
	addToFavorite: () => {
	},
	isFavorite: false,
}

export default Card;