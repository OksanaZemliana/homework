import {ReactComponent as FavoriteIcon} from './icons/favorite.svg'
import {ReactComponent as LogoIcon} from './icons/logo_book.svg'
import {ReactComponent as CartIcon} from './icons/cart.svg'
import "./Header.scss";
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";


const Header =({counterCart, counterFavorite})=> {

		return (
			<div className='wrapper-header'>
				<Link to={'/'}><LogoIcon /></Link>

				<div className='wrapper-counter'>
					<Link to={'/favorite'}>
					<span className='counter-header'>{counterFavorite}</span>
					<FavoriteIcon/>
					</Link>
				</div>
				<div className='wrapper-counter'>
					<Link to={'cart'}>
					<span className='counter-header'>{counterCart}</span>
					<CartIcon/>
					</Link>
				</div>
			</div>
		)
}

Header.propTypes = {
	counterCart: PropTypes.number,
	counterFavorite: PropTypes.number,
}

Header.defaultProps = {
	counterCart: 0,
	counterFavorite: 0,
}

export default Header;