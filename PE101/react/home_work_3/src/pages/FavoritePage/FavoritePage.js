import './FavoritePage.scss'
import Card from "../../components/WrapperCard/Card/Card";
import PropTypes from "prop-types";
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/Button";

const FavoritePage = ({
						  handleModal,
						  isOpenModal,
						  addToFavorite,
						  setCurrentCard,
						  cardsFavorite,
						  currentCard,
						  addToCart
					  }) => {
	return (
		<>
			<h1>Улюблені книги</h1>
			<ul className='list-favorite'>
				{cardsFavorite?.map(({url, name, author, price, id}) => (
					<li key={id}>
						<Card
							isFavorite={cardsFavorite?.some((el) => el.id === id)}
							setCurrentCard={setCurrentCard}
							addToFavorite={addToFavorite}
							id={id}
							url={url}
							name={name}
							author={author}
							price={price}
							handleModal={handleModal}
							isOpenModal={isOpenModal}
						/>
					</li>
				))}
			</ul>

			{isOpenModal && (
				<Modal closeModal={handleModal}
					   currentCard={currentCard}
					   isCloseButton
					   headerModal={'Ви дійсно хочете додати книгу в кошик:'}
					   action={
						   <div className='wrapper-btn'>
							   <Button type={"button"} className='btn-modal'
									   handleClick={() => {
										   {
											   addToCart(currentCard);
											   handleModal();
										   }
									   }}
							   >В кошик</Button>
							   <Button type={"button"} className='btn-modal'
									   handleClick={handleModal}>Відмінити</Button>
						   </div>
					   }
				/>)
			}
		</>
	)
}

FavoritePage.propTypes = {
	cardsFavorite: PropTypes.array,
	handleModal: PropTypes.func,
	isOpenModal: PropTypes.bool,
	setCurrentCard: PropTypes.func,
	addToFavorite: PropTypes.func,
	addToCart:PropTypes.func,
	currentCard: PropTypes.object,
}

FavoritePage.defaultProps = {
	cardsFavorite: [],
	setCurrentCard: () => {
	},
	addToFavorite: () => {
	},
	isOpenModal: false,
	handleModal: () => {
	},
	addToCart: () => {},
	currentCard: {},
}


export default FavoritePage;