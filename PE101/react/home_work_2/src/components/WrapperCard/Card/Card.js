import {PureComponent} from "react";
import Button from "../../Button/Button";
import {ReactComponent as StarIcon} from './icons/star.svg'
import {ReactComponent as StarIconFill} from './icons/star_fill.svg'
import PropTypes from "prop-types";
import './Card.scss'


class Card extends PureComponent {

	render() {
		const {
			id,
			url,
			name,
			author,
			price,
			openModal,
			setCurrentCard,
			addToFavorite,
			isFavorite,
		} = this.props


		return (
			<div className='card'>
				<img src={url} alt={name}/>
				<Button className={'btn-star'} handleClick={() => {
					addToFavorite({id, name})}}>
					{!isFavorite ? <StarIcon/> :<StarIconFill className='icon-starFill'/>}
				</Button>


				<h2>{name}</h2>
				<p>{author}</p>
				<span>{price}</span>
				<Button type={'button'} handleClick={() => {
					openModal();
					setCurrentCard({id, name})
				}}
						className={'btn-card'}>До кошика</Button>
			</div>
		)
	}

}

Card.propTypes = {
	url: PropTypes.string,
	name: PropTypes.string,
	author: PropTypes.string,
	price: PropTypes.string,
	openModal: PropTypes.func,
	setCurrentCard: PropTypes.func,
	addToFavorite: PropTypes.func,
	isFavorite: PropTypes.bool,
}

Card.defaultProps = {
	url: '',
	name: '',
	author: '',
	price: '',
	openModal: () => {
	},
	setCurrentCard: () => {
	},
	addToFavorite: () => {
	},
	isFavorite: false,
}

export default Card;