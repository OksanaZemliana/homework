import {PureComponent} from "react";
import Card from "./Card/Card";
import PropTypes from "prop-types";
import './WrapperCard.scss'

class WrapperCard extends PureComponent {

	render() {
		const {openModal, cards, addToFavorite, setCurrentCard, isOpenModal, cardsFavorite} = this.props;
		return (
			<div>
				<ul className='list'>
					{cards.map(({url, name, author, price, sku}) => (
						<li key={sku}>
							<Card
								isFavorite={cardsFavorite?.some((el) => el.id === sku)}
								setCurrentCard={setCurrentCard}
								addToFavorite={addToFavorite}
								id={sku}
								url={url}
								name={name}
								author={author}
								price={price}
								openModal={openModal}
								isOpenModal={isOpenModal}
							/>
						</li>
					))}
				</ul>
			</div>
		)
	}

}

Card.propTypes = {
	url: PropTypes.string,
	name: PropTypes.string,
	author: PropTypes.string,
	price: PropTypes.string,
	sku: PropTypes.string,
	openModal: PropTypes.func,
	addToFavorite: PropTypes.func,
	setCurrentCard: PropTypes.func,

}

Card.defaultProps = {
	url: '',
	name: '',
	author: '',
	price: '',
	sku: "",
	openModal: () => {
	},
	addToFavorite: () => {
	},
	setCurrentCard: () => {
	},
}


export default WrapperCard;



