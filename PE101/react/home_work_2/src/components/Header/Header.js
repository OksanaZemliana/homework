import {PureComponent} from "react";
import {ReactComponent as FavoriteIcon} from './icons/favorite.svg'
import {ReactComponent as LogoIcon} from './icons/logo_book.svg'
import {ReactComponent as CartIcon} from './icons/cart.svg'
import "./Header.scss";
import PropTypes from 'prop-types';


class Header extends PureComponent {

	render() {
		const {counterCart, counterFavorite} = this.props;
		return (
			<div className='wrapper-header'>
				<LogoIcon/>
				<div className='wrapper-counter'>
					<span className='counter-header'>{counterFavorite}</span>
					<FavoriteIcon/>
				</div>
				<div className='wrapper-counter'>
					<span className='counter-header'>{counterCart}</span>
					<CartIcon/>
				</div>
			</div>
		)
	}

}

Header.propTypes = {
	counterCart: PropTypes.number,
	counterFavorite: PropTypes.number,
}

Header.defaultProps = {
	counterCart: 0,
	counterFavorite: 0,
}

export default Header;