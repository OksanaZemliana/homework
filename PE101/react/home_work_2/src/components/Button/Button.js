import {PureComponent} from "react";
import PropTypes from "prop-types";

class Button extends PureComponent {

	render (){
		const {type, children, className, handleClick} = this.props;

	return(
			<button type={type} className={className} onClick={handleClick}>
				{children}
			</button>
		)
	}

}

Button.propTypes = {
	children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]).isRequired,
	type: PropTypes.oneOf(['button', 'submit', 'reset']),
	className: PropTypes.string,
	handleClick: PropTypes.func,
};

Button.defaultProps = {
	handleClick: () => {},
	type: 'button',
	className: '',

};

export default Button;