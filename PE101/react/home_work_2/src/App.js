import {Component} from "react";
import './styles/App.scss'
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";
import Header from "./components/Header/Header";
import WrapperCard from "./components/WrapperCard/WrapperCard";
import Footer from "./components/Footer/Footer";

class App extends Component {

	state = {
		isOpenModal: false,
		cards: [],
		currentCard: {},
		counterCart: 0,
		cardsFavorite: [],
	}


	handleModal = () => {
		this.setState((prev) => ({...prev, isOpenModal: !prev.isOpenModal}))
	}

	async componentDidMount() {
		const items = await fetch('./data.json').then(res => res.json());
		this.setState({cards: items});

		if (localStorage.getItem('counterCart')) {
			this.setState({counterCart: +localStorage.getItem('counterCart')})
		}
		const cardsFavorite = localStorage.getItem('favoriteItems')
		if (cardsFavorite) {
			this.setState({cardsFavorite: JSON.parse(cardsFavorite)})
		}

	}

	setCurrentCard = (value) => {
		this.setState({currentCard: value})
	}

	addToCard = () => {
		this.setState(prev => {
			localStorage.setItem('counterCart', prev.counterCart + 1);
			return {counterCart: prev.counterCart + 1}
		})
	}

	addToFavorite = (item) => {
		this.setState((prev) => {
			let carts = [...prev.cardsFavorite]
			const itemFind = carts.find((el) => el.id === item.id)
			if (!itemFind) {
				carts.push(item)
			} else {
				carts = carts.filter((el) => el.id !== item.id)
			}

			localStorage.setItem(`favoriteItems`, JSON.stringify(carts));
			return {cardsFavorite: prev.cardsFavorite = carts}
		})
	}

	render() {
		const {isOpenModal, cards, currentCard, cardsFavorite, counterCart} = this.state;

		return (
			<>
				<Header counterCart={counterCart} counterFavorite={cardsFavorite.length}/>
				{cards && <WrapperCard cards={cards}
									   openModal={this.handleModal}
									   addToFavorite={this.addToFavorite}
									   setCurrentCard={this.setCurrentCard}
									   isOpenModal={isOpenModal}
									   cardsFavorite={cardsFavorite}
				/>}
				<Footer/>

				{isOpenModal && (
					<Modal closeModal={this.handleModal}
						   currentCard={currentCard}
						   isCloseButton
						   headerModal={'Ви дійсно хочете додати книгу в кошик:'}
						   action={
							   <div className='wrapper-btn'>
								   <Button type={"button"} className='btn-modal'
										   handleClick={() => {
											   {
												   this.addToCard();
												   this.handleModal()
											   }
										   }}

								   >В кошик</Button>
								   <Button type={"button"} className='btn-modal'
										   handleClick={this.handleModal}>Відмінити</Button>
							   </div>
						   }
					/>
				)
				}</>
		);
	}

}

export default App;
