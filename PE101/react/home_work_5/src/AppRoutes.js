import {Route, Routes} from "react-router-dom";
import WrapperCard from "./components/WrapperCard/WrapperCard";
import CartPage from "./pages/CartPage/CartPage";
import FavoritePage from "./pages/FavoritePage/FavoritePage";
import NotFoundPage from "./pages/NotFoundPage/NotFoundPage";


const AppRoutes = () => {
	return (
		<Routes>
			<Route index element={<WrapperCard/>}/>
			<Route path={"/cart"} element={<CartPage/>}/>
			<Route path={"/favorite"} element={<FavoritePage/>}/>
			<Route path={"*"} element={<NotFoundPage/>}/>
		</Routes>
	)
}
export default AppRoutes;