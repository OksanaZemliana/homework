import './styles/App.scss'
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import AppRoutes from "./AppRoutes";
import {useEffect} from "react";
import {actionFetchCards} from "./store/slices/card.slice";
import setFromLS from "./helpers/setFromLS";
import {useDispatch} from "react-redux";

const App = () => {
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(actionFetchCards());
		dispatch(setFromLS());
	}, []);

	return (
		<>
			<Header/>
			<section>
				<AppRoutes/>
			</section>
			<Footer/>
		</>
	);
}

export default App;
