import {Formik, Form} from "formik";
import {validationSchema} from "./validation"
import './Form.scss'
import Input from "./Input/Input";
import Textarea from "./Textarea/Textarea";
import {actionButton, actionForm} from "../../store/slices/app.slice";
import {useDispatch, useSelector} from "react-redux";
import {actionSetItems} from '../../store/slices/cart.slice'
import Button from "../Button/Button";
import PropTypes from "prop-types";


const FormCart = ({totalAmount, handleTitleSubmit}) => {
	const dispatch = useDispatch();
	const itemsBuy = useSelector(store => store.carts.items)

	const handleSubmitForm = (values, {resetForm}) => {
		console.log(`user info  ${JSON.stringify(values)}`);
		console.log(`buy items  ${JSON.stringify(itemsBuy)}`);
		console.log(`total amount is: ${totalAmount}`)
		resetForm();
		handleTitleSubmit();
		dispatch(actionForm(false));
		dispatch(actionSetItems([]));

	}

	const initialValues = {
		name: '',
		lastName: '',
		age: '',
		address: '',
		phone: ''
	}

	return (
		<Formik initialValues={initialValues}
				onSubmit={handleSubmitForm}
				validationSchema={validationSchema}>

			{({isValid,}) => (
				<Form className='wrapper-form'>
					<div className='form'>
						<h3>Оформити замовлення</h3>
						<Input name='name' type='text' placeholder="Введіть Ваше ім'я" label="Ваше ім'я"
							   className={'form-label'}/>
						<Input name='lastName' type='text' placeholder="Введіть Ваше прізвище" label="Ваше прізвище"
							   className={'form-label'}/>
						<Input name='age' type='text' placeholder="Введіть Ваш вік" label="Ваш вік"
							   className={'form-label'}/>
						<Textarea name='address' type='text' placeholder="Введіть Вашу адресу" label="Ваша адреса"
								  className={'form-label'} rows={3}/>
						<Input name='phone' type='text' placeholder="Введіть Ваш телефон" label="Ваш номер телефону"
							   className={'form-label'}/>

						<Button type='submit' disabled={!isValid} className='btn-form'>Підтвердити замовлення</Button>
						<Button type='button' className='btn-form' handleClick={() => {
							dispatch(actionForm(false));
							dispatch(actionButton(true));
						}}>Скасувати оформлення</Button>
					</div>
				</Form>
			)}
		</Formik>
	)
}


FormCart.propTypes = {
	totalAmount: PropTypes.number,
	handleTitleSubmit: PropTypes.func,
}

FormCart.defaultProps = {
	totalAmount: 0,
	handleTitleSubmit: () => {
	},

}
export default FormCart;

