import {ReactComponent as FavoriteIcon} from './icons/favorite.svg'
import {ReactComponent as LogoIcon} from './icons/logo_book.svg'
import {ReactComponent as CartIcon} from './icons/cart.svg'
import "./Header.scss";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";


const Header = () => {
	const cardsFavorite = useSelector(store => store.favorite.data)
	const carts = useSelector(store => store.carts.items)
	const counterCart = carts?.map(({count}) => count).reduce((prev, curr) => prev + curr, 0);

	return (
		<div className='wrapper-header'>
			<Link to={'/'}><LogoIcon/></Link>

			<div className='wrapper-counter'>
				<Link to={'/favorite'}>
					<span className='counter-header'>{cardsFavorite.length}</span>
					<FavoriteIcon/>
				</Link>
			</div>
			<div className='wrapper-counter'>
				<Link to={'cart'}>
					<span className='counter-header'>{counterCart}</span>
					<CartIcon/>
				</Link>
			</div>
		</div>
	)
}

export default Header;