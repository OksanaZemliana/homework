import "../../App";
import './Modal.scss'
import PropTypes from 'prop-types';
import {useDispatch, useSelector} from "react-redux";
import {actionModal} from "../../store/slices/app.slice";

const Modal = (props) => {

	const {headerModal, isCloseButton, action,} = props;
	const dispatch = useDispatch();
	const currentCard = useSelector(store => store.app.currentData);
	const isOpenModal = useSelector(store => store.app.isOpenModal);

	function handleOutsideClick({target}) {
		if (!target.closest(".modal")) {
			return dispatch(actionModal(false));
		}
	}

	return (
		<div className='wrapperModal' onClick={handleOutsideClick}>
			<div className='modal'>
				{isCloseButton &&
					<svg onClick={() => {
						dispatch(actionModal(false))
					}} className='iconModal' xmlns="http://www.w3.org/2000/svg" height="48" viewBox="0 96 960 960"
						 width="48">
						<path
							d="m336 762 144-144 144 144 42-42-144-144 144-144-42-42-144 144-144-144-42 42 144 144-144 144 42 42ZM180 936q-24 0-42-18t-18-42V276q0-24 18-42t42-18h600q24 0 42 18t18 42v600q0 24-18 42t-42 18H180Zm0-60h600V276H180v600Zm0-600v600-600Z"/>
					</svg>}
				<h2>{headerModal}</h2>
				<p>"{currentCard.name}" ? </p>
				{action}
			</div>
		</div>
	)
}

Modal.propTypes = {
	headerModal: PropTypes.string,
	isCloseButton: PropTypes.bool,
	action: PropTypes.object.isRequired,

}
Modal.defaultProps = {
	headerModal: "",
	isCloseButton: false,
}

export default Modal;