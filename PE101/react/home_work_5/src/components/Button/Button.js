import PropTypes from "prop-types";

const Button = (props) => {

	const {type, children, className, handleClick, disabled} = props;

	return (
		<button type={type} className={className} onClick={handleClick} disabled={disabled}>
			{children}
		</button>
	)
}

Button.propTypes = {
	children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]).isRequired,
	type: PropTypes.oneOf(['button', 'submit', 'reset']),
	className: PropTypes.string,
	handleClick: PropTypes.func,
	disabled: PropTypes.bool,
};

Button.defaultProps = {
	handleClick: () => {
	},
	type: 'button',
	className: '',
	disabled: false,


};

export default Button;