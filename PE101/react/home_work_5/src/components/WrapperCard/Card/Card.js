import Button from "../../Button/Button";
import {ReactComponent as StarIcon} from './icons/star.svg'
import {ReactComponent as StarIconFill} from './icons/star_fill.svg'
import PropTypes from "prop-types";
import './Card.scss'
import {useDispatch} from "react-redux";
import {actionAddToFavorite} from '../../../store/slices/favorite.slice'
import {actionButton, actionModal, actionSetCurrentData} from "../../../store/slices/app.slice";


const Card = (props) => {
	const {
		id,
		url,
		name,
		author,
		price,
		isFavorite,
	} = props

	const dispatch = useDispatch();
	const openModal = () => dispatch(actionModal(true));

	return (
		<div className='card'>
			<img src={url} alt={name}/>
			<Button className={'btn-star'} handleClick={() => {
				dispatch(actionAddToFavorite({id, name, url, price, author}))
			}}>
				{!isFavorite ? <StarIcon/> : <StarIconFill className='icon-starFill'/>}
			</Button>


			<h2>{name}</h2>
			<p>{author}</p>
			<span>{price} грн</span>
			<Button type={'button'} handleClick={() => {
				openModal();
				dispatch(actionSetCurrentData({id, name, url, price, author}))
				dispatch(actionButton(true));
			}}
					className={'btn-card'}>До кошика</Button>
		</div>
	)
}

Card.propTypes = {
	id: PropTypes.string,
	url: PropTypes.string,
	name: PropTypes.string,
	author: PropTypes.string,
	price: PropTypes.string,
	isFavorite: PropTypes.bool,
}

Card.defaultProps = {
	id: '',
	url: '',
	name: '',
	author: '',
	price: '',
	isFavorite: false,
}

export default Card;