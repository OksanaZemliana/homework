import Card from "./Card/Card";
import './WrapperCard.scss'
import Modal from "../Modal/Modal";
import Button from "../Button/Button";
import {useDispatch, useSelector} from "react-redux";
import {actionAddToCart} from "../../store/slices/cart.slice";
import {actionModal} from "../../store/slices/app.slice";

const WrapperCard = () => {

	const dispatch = useDispatch();
	const cards = useSelector(store => store.cards.data);
	const cardsFavorite = useSelector(store => store.favorite.data);
	const isOpenModal = useSelector(store => store.app.isOpenModal);
	const currentCard = useSelector(store => store.app.currentData);
	const closeModal = () => dispatch(actionModal(false));


	return (
		<div>
			cards && {<ul className='list'>
			{cards.map(({url, name, author, price, id}) => (
				<li key={id}>
					<Card
						isFavorite={cardsFavorite?.some((el) => el.id === id)}
						id={id}
						url={url}
						name={name}
						author={author}
						price={price}
					/>
				</li>
			))}
		</ul>}

			{isOpenModal && (
				<Modal isCloseButton
					   headerModal={'Ви дійсно хочете додати книгу в кошик:'}
					   action={
						   <div className='wrapper-btn'>
							   <Button type={"button"} className='btn-modal'
									   handleClick={() => {
										   {
											   dispatch(actionAddToCart(currentCard))
											   closeModal();
										   }
									   }}
							   >В кошик</Button>
							   <Button type={"button"} className='btn-modal'
									   handleClick={closeModal}>Відмінити</Button>
						   </div>
					   }
				/>)
			}
		</div>
	)
}

export default WrapperCard;



