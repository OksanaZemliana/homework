import {createSlice} from "@reduxjs/toolkit";

const cartSlice = createSlice({
	name: "carts",
	initialState: {
		items: [],

	},
	reducers : {

        actionSetItems: (state, action) => {
			state.items = action.payload;
			localStorage.setItem("carts", JSON.stringify(state.items))
		},
		actionAddToCart:(state, action) =>  {
			const index = state.items.findIndex(el => el.id === action.payload.id)
			if (index === -1) {
				state.items.push({...action.payload, count: 1})
			} else {
				state.items[index].count += 1
			}
			localStorage.setItem("carts", JSON.stringify(state.items))
		},

		actionIncrementCartItem: (state, action) => {
			const index = state.items.findIndex(el => el.id === action.payload)
			if (index !== -1) {
				state.items[index].count += 1
			}
			localStorage.setItem("carts", JSON.stringify(state.items))
		},

		actionDecrementCartItem : (state, action) => {
			const index = state.items.findIndex(el => el.id === action.payload)
			if (index !== -1 && state.items[index].count > 1) {
				state.items[index].count -= 1
			}

			localStorage.setItem("carts", JSON.stringify(state.items))
		},
		actionDeleteCartItem : (state, action) => {
			const index = state.items.findIndex(el => el.id === action.payload.id)

			if (index !== -1) {
				state.items.splice(index, 1);
			}
			localStorage.setItem("carts", JSON.stringify(state.items))
		}
	}
})

export const {actionAddToCart, actionIncrementCartItem, actionDecrementCartItem, actionDeleteCartItem, actionSetItems} = cartSlice.actions

export default cartSlice.reducer