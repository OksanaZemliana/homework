import gulp from "gulp";
import htmlmin from "gulp-htmlmin";
import concat from "gulp-concat";
import imageMin from "gulp-imagemin";
import prefix from "gulp-autoprefixer";
import terser from "gulp-terser";
import cleanCss from "gulp-clean-css";
import clean from "gulp-clean";
import browserSync from "browser-sync";

const sync = browserSync.create();

export const html = () => gulp.src('./src/**/*.html')
    .pipe(htmlmin())
    .pipe(gulp.dest('./dist'));

export const css = () => gulp.src('./src/**/*.styles')
    .pipe(concat('style.min.styles'))
    .pipe(prefix())
    .pipe(cleanCss())
    .pipe(gulp.dest('./dist'))

export const js = () => gulp.src('./src/**.*.js')
    .pipe(concat('script.min.js'))
    .pipe(terser())
    .pipe(gulp.dest('./dist/'))

const images = () => gulp.src('./src/images/**.*')
    .pipe(imageMin())
    .pipe(gulp.dest('./dist'))

export const cleanDest = () => gulp.src('./dist/*',{read: false}).pipe(clean());

export const build =() => gulp.series(cleanDest, gulp.parallel(html, css, js, images));

export const dev = () => {
    sync.init({
        server: {
            baseDir: "./dist/"
        }
    });

    gulp.watch('./src/**/*', gulp.series(build, (done) => {
        sync.reload();
        done();
    }));

}



