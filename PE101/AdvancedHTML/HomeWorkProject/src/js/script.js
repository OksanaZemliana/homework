const navigationBtn = document.querySelector('.navigation-btn');
const navList = document.querySelector('.navigation__list');

navigationBtn.addEventListener('click', ()=> {
   const  navBars = document.querySelectorAll('.navigation-btn__bar');
    navBars.forEach(el=>el.classList.toggle('navigation-btn__bar--active'));
    navList.classList.toggle('navigation__list--active');
});
