import gulp from "gulp";
const { src, dest, watch, series, parallel } = gulp;
import autoprefixer from "gulp-autoprefixer";
import cleanCSS from "gulp-clean-css";

import browserSync from "browser-sync";
const bsServer = browserSync.create();

// const sass = require('gulp-sass')(require('sass'));
import dartSass from "sass";
import gulpSass from "gulp-sass";
const sass = gulpSass(dartSass);

function serve() {
    bsServer.init({
        server: {
            baseDir: "./",
            browser: "firefox",
        },
    });
}

function styles() {
    return (
        src("./src/styles/style.sss")
            .pipe(sass().on("error", sass.logError))
            .pipe(
                autoprefixer(["last 15 versions", "> 1%", "ie 8", "ie 7"], {
                    cascade: true,
                })
            )
            // .pipe(cleanCSS({ compatibility: "ie8" }))
            .pipe(dest("./dist/css/"))
            .pipe(bsServer.reload({ stream: true }))
    );
}

function watcher() {
    watch("./src/styles/**/*.scss", styles);
}

export const dev = series(styles, parallel(serve, watcher));



